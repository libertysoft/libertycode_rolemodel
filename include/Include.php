<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/subject/api/SubjectInterface.php');

include($strRootPath . '/src/permission/library/ConstPermission.php');
include($strRootPath . '/src/permission/exception/PermissionKeyNotFoundException.php');
include($strRootPath . '/src/permission/model/PermissionEntity.php');
include($strRootPath . '/src/permission/model/PermissionEntityCollection.php');
include($strRootPath . '/src/permission/model/PermissionEntityFactory.php');

include($strRootPath . '/src/permission/subject/library/ConstSubjPerm.php');
include($strRootPath . '/src/permission/subject/exception/TabSubjectInvalidFormatException.php');
include($strRootPath . '/src/permission/subject/api/PermissionSubjectInterface.php');
include($strRootPath . '/src/permission/subject/model/SubjPermEntity.php');
include($strRootPath . '/src/permission/subject/model/SubjPermEntityCollection.php');
include($strRootPath . '/src/permission/subject/model/SubjPermEntityFactory.php');
include($strRootPath . '/src/permission/subject/model/repository/SubjPermEntityRepository.php');
include($strRootPath . '/src/permission/subject/model/repository/SubjPermEntityCollectionRepository.php');

include($strRootPath . '/src/permission/subject/sql/library/ConstSqlSubjPerm.php');
include($strRootPath . '/src/permission/subject/sql/model/SqlSubjPermEntity.php');
include($strRootPath . '/src/permission/subject/sql/model/SqlSubjPermEntityFactory.php');
include($strRootPath . '/src/permission/subject/sql/model/repository/SqlSubjPermEntityRepository.php');
include($strRootPath . '/src/permission/subject/sql/model/repository/SqlSubjPermEntityCollectionRepository.php');

include($strRootPath . '/src/role/permission/library/ConstRolePerm.php');
include($strRootPath . '/src/role/permission/model/RolePermEntity.php');
include($strRootPath . '/src/role/permission/model/RolePermEntityCollection.php');
include($strRootPath . '/src/role/permission/model/RolePermEntityFactory.php');
include($strRootPath . '/src/role/permission/model/repository/RolePermEntityRepository.php');
include($strRootPath . '/src/role/permission/model/repository/RolePermEntityCollectionRepository.php');

include($strRootPath . '/src/role/permission/sql/library/ConstSqlRolePerm.php');
include($strRootPath . '/src/role/permission/sql/model/SqlRolePermEntity.php');
include($strRootPath . '/src/role/permission/sql/model/SqlRolePermEntityFactory.php');
include($strRootPath . '/src/role/permission/sql/model/repository/SqlRolePermEntityRepository.php');
include($strRootPath . '/src/role/permission/sql/model/repository/SqlRolePermEntityCollectionRepository.php');

include($strRootPath . '/src/role/library/ConstRole.php');
include($strRootPath . '/src/role/exception/NameNotFoundException.php');
include($strRootPath . '/src/role/exception/TabSubjectInvalidFormatException.php');
include($strRootPath . '/src/role/model/RoleEntity.php');
include($strRootPath . '/src/role/model/RoleEntityCollection.php');
include($strRootPath . '/src/role/model/RoleEntityFactory.php');
include($strRootPath . '/src/role/model/repository/RoleEntityRepository.php');
include($strRootPath . '/src/role/model/repository/RoleEntityCollectionRepository.php');

include($strRootPath . '/src/role/sql/library/ConstSqlRole.php');
include($strRootPath . '/src/role/sql/model/SqlRoleEntity.php');
include($strRootPath . '/src/role/sql/model/SqlRoleEntityFactory.php');
include($strRootPath . '/src/role/sql/model/repository/SqlRoleEntityRepository.php');
include($strRootPath . '/src/role/sql/model/repository/SqlRoleEntityCollectionRepository.php');

include($strRootPath . '/src/role/subject/api/RoleSubjectInterface.php');

include($strRootPath . '/src/role/browser/library/ConstRoleBrowser.php');
include($strRootPath . '/src/role/browser/model/RoleBrowserEntity.php');
include($strRootPath . '/src/role/browser/model/repository/RoleBrowserRepository.php');

include($strRootPath . '/src/role/browser/sql/library/ConstSqlRoleBrowser.php');
include($strRootPath . '/src/role/browser/sql/model/repository/SqlRoleBrowserRepository.php');