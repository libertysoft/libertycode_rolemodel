LibertyCode_RoleModel
=====================



Description
-----------

Library contains permission and role model components, 
to manage access.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
--------------------------------------------

1. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all standard rules, 
        added on its rule collection.
        
    - Validator rules implementation (or equivalent): 
    
        Validator provided on entities, 
        must contain all validator rules, 
        added on its rule collection.
        Each validator rule must use validator,
        with same implementation as validator provided on entities.

2. Library repository: liberty_code/model: version 1.0

    - Validation entity rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all entity rules, 
        added on its rule collection.

3. Library repository: liberty_code/sql: version 1.0

    - Validation SQL rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all SQL rules, 
        added on its rule collection.
        
---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/role_model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/role_model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Subject

Subject allows to design subject base, 
can be used in role and permission entities.

_Elements_

- Subject

    Allows to design subject base,
    for role and permission entities.
    
#### Permission

Permission entity allows to design permission, 
using entity.

_Elements_

- PermissionEntity

    Extends save configured entity features. 
    Uses entity to design a permission,
    to authorize specific operations.

- PermissionEntityCollection

    Extends fixed entity collection features. 
    Allows to design collection of permission entities.

- PermissionEntityFactory

    Extends fixed entity factory features. 
    Allows to design permission factory, 
    to provide permission entity objects,
    from specified configuration.
    
- SubjPermEntity

    Extends permission entity features. 
    Relates to a specific subject.
    
- SubjPermEntityCollection

    Extends permission entity collection features. 
    Used to store subject permission entities.
    
- SubjPermEntityFactory

    Extends permission entity factory features. 
    Provides subject permission entity objects.
    
- SubjPermEntityRepository

    Extends fixed multi repository features.
    Allows to design subject permission entity repository,
    to save data, from subject permission entity, in persistence.

- SubjPermEntityCollectionRepository

    Extends fixed multi collection repository features. 
    Allows to design subject permission entity collection repository,
    to save data, from subject permission entity collection, in persistence.

- SqlSubjPermEntity

    Extends subject permission entity features. 
    Uses SQL rules for attributes validation.

- SqlSubjPermEntityFactory

    Extends subject permission entity factory features. 
    Provides SQL subject permission entity objects.
    
- SqlSubjPermEntityRepository

    Extends subject permission entity repository features.
    Uses SQL table persistor, as persistence. 
    
- SqlSubjPermEntityCollectionRepository

    Extends subject permission entity collection repository features. 
    Uses SQL table persistor, as persistence. 
    
```php
// Get subject permission factory
use liberty_code\role_model\permission\subject\model\SubjPermEntityFactory;
$subjPermEntityFactory = new SubjPermEntityFactory($provider);
...
// Get permission builder
use liberty_code\role_model\permission\build\model\DefaultBuilder;
$permissionBuilder = new DefaultBuilder($permSpec, $subjPermEntityFactory);
...
// Get subject permission collection
use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection;
$subjPermEntityCollection = new SubjPermEntityCollection();
...
// Hydrate subject permission collection
$permissionBuilder->hydratePermissionCollection($subjPermEntityCollection);
...
foreach($subjPermEntityCollection->getTabPermissionKey() as $permissionKey) {
    echo($subjPermEntityCollection->getObjPermission($permissionKey)->getStrPermissionKey() .'<br />');
}
/**
 * Show: 
 * Permission key 1
 * ...
 * Permission key N
 */
...
```

#### Permission subject

Permission subject allows to design permission subject base, 
can be used in subject permission entities.

_Elements_

- PermissionSubject

    Extends permission subject and subject features. 
    Allows to design a permission subject,
    can be used in subject permission entities.
    
#### Role

Role entity allows to design role, 
using entity.

_Elements_

- RolePermEntity

    Extends permission entity features. 
    Relates to a specific role.
    
- RolePermEntityCollection

    Extends permission entity collection features. 
    Used to store role permission entities.
    
- RolePermEntityFactory

    Extends permission entity factory features. 
    Provides role permission entity objects.
    
- RolePermEntityRepository

    Extends fixed multi repository features.
    Allows to design role permission entity repository,
    to save data, from role permission entity, in persistence.

- RolePermEntityCollectionRepository

    Extends fixed multi collection repository features. 
    Allows to design role permission entity collection repository,
    to save data, from role permission entity collection, in persistence.
    
- SqlRolePermEntity

    Extends role permission entity features. 
    Uses SQL rules for attributes validation.
    
- SqlRolePermEntityFactory

    Extends role permission entity factory features. 
    Provides SQL role permission entity objects.
    
- SqlRolePermEntityRepository

    Extends role permission entity repository features.
    Uses SQL table persistor, as persistence. 
    
- SqlRolePermEntityCollectionRepository

    Extends role permission entity collection repository features. 
    Uses SQL table persistor, as persistence. 
    
- RoleEntity

    Extends save configured entity features. 
    Uses entity to design a role.

- RoleEntityCollection

    Extends fixed entity collection features. 
    Allows to design collection of role entities.

- RoleEntityFactory

    Extends fixed entity factory features. 
    Allows to design role factory, 
    to provide role entity objects,
    from specified configuration.
    
- RoleEntityRepository

    Extends fixed multi repository features.
    Allows to design role entity repository,
    to save data, from role entity, in persistence.

- RoleEntityCollectionRepository

    Extends fixed multi collection repository features. 
    Allows to design role entity collection repository,
    to save data, from role entity collection, in persistence.
    
- SqlRoleEntity

    Extends role entity features. 
    Uses SQL rules for attributes validation.
    
- SqlRoleEntityFactory

    Extends role entity factory features. 
    Provides SQL role entity objects.
    
- SqlRoleEntityRepository

    Extends role entity repository features.
    Uses SQL table persistor, as persistence. 
    
- SqlRoleEntityCollectionRepository

    Extends role entity collection repository features. 
    Uses SQL table persistor, as persistence. 

```php
// Get role permission factory
use liberty_code\role_model\role\permission\model\RolePermEntityFactory;
$rolePermEntityFactory = new RolePermEntityFactory($provider);
...
// Get permission builder
use liberty_code\role_model\permission\build\model\DefaultBuilder;
$permissionBuilder = new DefaultBuilder($permSpec, $rolePermEntityFactory);
...
// Get role factory
use liberty_code\role_model\role\model\RoleEntityFactory;
$roleEntityFactory = new RoleEntityFactory();
...
// Get role builder
use liberty_code\role\role\build\model\DefaultBuilder;
$roleBuilder = new DefaultBuilder(
    $roleEntityFactory, 
    array(
        $permissionBuilder
    )
);
...
// Get role collection
use liberty_code\role_model\role\model\RoleEntityCollection;
$roleEntityCollection = new RoleEntityCollection();
...
// Hydrate role collection
$roleBuilder->hydrateRoleCollection($roleEntityCollection);
...
foreach($roleEntityCollection->getTabRoleName() as $roleName) {
    echo($roleEntityCollection->getObjRole($roleName)->getStrRoleName() .'<br />');
}
/**
 * Show: 
 * Role name 1
 * ...
 * Role name N
 */
...
```

#### Role subject

Role subject allows to design role subject, 
can be used in role entities.

_Elements_

- RoleSubject

    Extends role subject and subject features. 
    Allows to design a role subject,
    can be used in role entities.
    
#### Role browser

Browser allows to provide role entities, 
from specified browsing configuration.

- RoleBrowserEntity

    Extends validator configured entity features.
    Allows to define attributes,
    to search role entities.

- RoleBrowserRepository

    Allows to load role entities,
    from specified role browser entity
    or specified browsing configuration.

- SqlRoleBrowserRepository

    Extends role browser repository features. 
    Uses SQL table browser, 
    to load role entities. 

```php
// Get role browser entity
use liberty_code\role_model\role\browser\model\RoleBrowserEntity;
$roleBrowserEntity = new RoleBrowserEntity(array(), $validator);
...
// Hydrate role browser entity
$roleBrowserEntity->intAttrItemCountPage = 50; // Select 50 roles per page
$roleBrowserEntity->intAttrPageIndex = 0; // Select Page 1
$roleBrowserEntity->strAttrCritLikeName = '...' // Select result where role name like '...'
$roleBrowserEntity->$strAttrSortName = 'asc' // Sort result by ascending role name
...
// Get role browser repository
use liberty_code\role_model\role\browser\sql\model\repository\SqlRoleBrowserRepository;
$roleBrowserRepository = new SqlRoleBrowserRepository(
    $sqlTableBrowser, 
    $sqlRoleEntityCollectionRepository
);
...
// Load role collection
$roleBrowserRepository->loadFromEntity($roleEntityCollection, $roleBrowserEntity);
...
```

---


