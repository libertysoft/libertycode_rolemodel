<?php
/**
 * This class allows to define SQL role entity factory class.
 * SQL role entity factory is role entity factory,
 * to provide new SQL role entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\sql\model;

use liberty_code\role_model\role\model\RoleEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\role\api\RoleInterface;
use liberty_code\role_model\role\sql\model\SqlRoleEntity;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityRepository;



/**
 * @method SqlRoleEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|SqlRoleEntity getObjRole(array $tabConfig = array(), string $strConfigKey = null, RoleInterface $objRole = null) @inheritdoc
 */
class SqlRoleEntityFactory extends RoleEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SqlRoleEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SqlRoleEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objRepository = $this->getObjInstance(SqlRoleEntityRepository::class);
        $result = new SqlRoleEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objRepository
        );

        // Return result
        return $result;
    }



}