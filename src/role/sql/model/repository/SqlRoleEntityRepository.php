<?php
/**
 * This class allows to define SQL role entity repository class.
 * SQL role entity repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\sql\model\repository;

use liberty_code\role_model\role\model\repository\RoleEntityRepository;

use liberty_code\model\repository\sub_repository\library\ToolBoxSubRepo;
use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\permission\library\ConstRolePerm;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityRepository;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityCollectionRepository;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\sql\library\ConstSqlRole;



/**
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlRoleEntityRepository extends RoleEntityRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TablePersistor $objPersistor
     * @param SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
     */
    public function __construct(
        TablePersistor $objPersistor,
        SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
    )
    {
        // Call parent constructor
        parent::__construct($objPersistor, $objRolePermEntityCollectionRepo);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return TablePersistor::class;
    }



    /**
     * Get SQL table name where roles stored.
     * Overwrite it to implement specific table name.
     *
     * @return string
     */
    public function getStrSqlTableName()
    {
        // Return result
        return ConstSqlRole::SQL_TABLE_NAME_ROLE;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array(
            ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME => $this->getStrSqlTableName(),
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID  => ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID,
        );
    }



    /**
     * @inheritdoc
     */
    protected function getCriteriaIdFromId($intId)
    {
        // Init var
        $result = null;
        /** @var SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo */
        $objRolePermEntityCollectionRepo = $this->objRolePermEntityCollectionRepo;
        $objSelectCommand = $this
            ->getObjPersistor()
            ->getObjCommandFactory()
            ->getObjSelectCommand();

        // Get SQL arguments
        $strTableNm = $this->getStrSqlTableName();
        $strRolePermTableNm = $objRolePermEntityCollectionRepo->getObjRepository()->getStrSqlTableName();

        // Build select command
        $objSelectCommand
            ->setClauseSelect(
                array(
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID,
                    ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID
                )
            )
            ->setClauseFrom(
                array(
                    [
                        'type' => 'left_outer',
                        'left' => $strTableNm,
                        'right' => $strRolePermTableNm,
                        'on' => [
                            'content' => [
                                [
                                    'operand' => [
                                        'table_name' => $strTableNm,
                                        'column_name' => ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID
                                    ],
                                    'value' => [
                                        'table_name' => $strRolePermTableNm,
                                        'column_name' => ConstRolePerm::ATTRIBUTE_NAME_SAVE_PERM_ROLE_ID
                                    ]
                                ]
                            ]
                        ]
                    ]
                )
            )
            ->setClauseWhere(
                array(
                    'content' => [
                        [
                            'operand' => ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID,
                            'operator' => 'equal',
                            'value' => ['value' => $intId]
                        ]
                    ]
                )
            );

        // Execute query
        $objDbResult = $objSelectCommand->executeResult();
        if($objDbResult !== false)
        {
            // Get criteria, if found
            $tabData = $objDbResult->getTabData();
            $tabCriteria = ToolBoxSubRepo::getTabCriteriaFromData($tabData, $this);
            $result = (
            isset($tabCriteria[0]) && is_array($tabCriteria[0]) ?
                $tabCriteria[0] :
                $result
            );

            // Close query
            $objDbResult->close();
        }

        // Return result
        return $result;
    }





    // Methods subject getters
    // ******************************************************************************

    /**
     * Get SQL table name where role references stored.
     * Overwrite it to implement specific table name.
     *
     * @return string
     */
    public function getStrSqlSubjectRoleRefTableName()
    {
        // Return result
        return ConstSqlRole::SQL_TABLE_NAME_ROLE_SUBJ;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set role structure.
     * It requires to set role permission structure previously
     * (@see SqlRolePermEntityRepository::setStructure() ).
     * Return true if correctly run, false else.
     *
     * @return boolean
     */
    public function setStructure()
    {
        // Init var
        $objConnection = $this->getObjPersistor()->getObjCommandFactory()->getObjConnection();
        /** @var SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo */
        $objRolePermEntityCollectionRepo = $this->objRolePermEntityCollectionRepo;
        $strTableNm = $this->getStrSqlTableName();
        $strRolePermTableNm = $objRolePermEntityCollectionRepo->getObjRepository()->getStrSqlTableName();

        // Create role table
        $strSql = sprintf(
            'CREATE TABLE %1$s (
                %2$s int(10) unsigned NOT NULL AUTO_INCREMENT,
                %3$s datetime NOT NULL,
                %4$s datetime NOT NULL,
                %5$s varchar(100) NOT NULL,
                PRIMARY KEY (%2$s),
                UNIQUE KEY %5$s (%5$s)
            );',
            $objConnection->getStrEscapeName($strTableNm),
            $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID),
            $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_DT_CREATE),
            $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_DT_UPDATE),
            $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_NM)
        );
        $result = ($objConnection->execute($strSql) !== false);

        // Update role permission table, if required
        if($result)
        {
            $strSql = sprintf(
                'ALTER TABLE %1$s 
                ADD FOREIGN KEY (%2$s) REFERENCES %3$s (%4$s);',
                $objConnection->getStrEscapeName($strRolePermTableNm),
                $objConnection->getStrEscapeName(ConstRolePerm::ATTRIBUTE_NAME_SAVE_PERM_ROLE_ID),
                $objConnection->getStrEscapeName($strTableNm),
                $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID)
            );
            $result = ($objConnection->execute($strSql) !== false);
        }

        // Create role subject table, if required
        if($result)
        {
            $strSql = sprintf(
                'CREATE TABLE %1$s (
                %2$s int(10) unsigned NOT NULL AUTO_INCREMENT,
                %3$s int(10) unsigned NOT NULL,
                %4$s varchar(100) NOT NULL,
                %5$s varchar(100) NOT NULL,
                PRIMARY KEY (%2$s),
                FOREIGN KEY (%3$s) REFERENCES %6$s (%7$s)
            );',
                $objConnection->getStrEscapeName($this->getStrSqlSubjectRoleRefTableName()),
                $objConnection->getStrEscapeName(ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ID),
                $objConnection->getStrEscapeName(ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID),
                $objConnection->getStrEscapeName(ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE),
                $objConnection->getStrEscapeName(ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID),
                $objConnection->getStrEscapeName($this->getStrSqlTableName()),
                $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID)
            );
            $result = ($objConnection->execute($strSql) !== false);
        }

        // Create indexes, if required
        if($result)
        {
            // Create subject type index
            $strSql = sprintf(
                'CREATE INDEX %1$s ON %2$s (%1$s);',
                $objConnection->getStrEscapeName(ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE),
                $objConnection->getStrEscapeName($this->getStrSqlSubjectRoleRefTableName())
            );
            $result = ($objConnection->execute($strSql) !== false);

            // Create subject id index
            $strSql = sprintf(
                'CREATE INDEX %1$s ON %2$s (%1$s);',
                $objConnection->getStrEscapeName(ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID),
                $objConnection->getStrEscapeName($this->getStrSqlSubjectRoleRefTableName())
            );
            $result = ($objConnection->execute($strSql) !== false) && $result;
        }

        // Return result
        return $result;
    }



}