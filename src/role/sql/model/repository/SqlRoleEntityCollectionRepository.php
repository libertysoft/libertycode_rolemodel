<?php
/**
 * This class allows to define SQL role entity collection repository class.
 * SQL role entity collection repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\sql\model\repository;

use liberty_code\role_model\role\model\repository\RoleEntityCollectionRepository;

use liberty_code\model\repository\sub_repository\library\ToolBoxSubRepo;
use liberty_code\role_model\role\model\RoleEntity;
use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\permission\library\ConstRolePerm;
use liberty_code\role_model\role\permission\model\RolePermEntityCollection;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityCollectionRepository;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\model\RoleEntityFactory;
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\exception\TabSubjectInvalidFormatException;
use liberty_code\role_model\role\sql\library\ConstSqlRole;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityRepository;
use liberty_code\role_model\role\subject\api\RoleSubjectInterface;



/**
 * @method null|SqlRoleEntityRepository getObjRepository() @inheritdoc
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlRoleEntityCollectionRepository extends RoleEntityCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Role entity collection instance.
     * @var null|RoleEntityCollection
     */
    protected $objRoleEntityCollection;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlRoleEntityRepository $objRepository
     * @param SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
     * @param RoleEntityCollection $objRoleEntityCollection = null
     */
    public function __construct(
        RoleEntityFactory $objEntityFactory,
        SqlRoleEntityRepository $objRepository,
        RolePermEntityCollection $objRolePermEntityCollection,
        SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo,
        RoleEntityCollection $objRoleEntityCollection = null
    )
    {
        // Init properties
        $this->objRoleEntityCollection = $objRoleEntityCollection;

        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository,
            $objRolePermEntityCollection,
            $objRolePermEntityCollectionRepo
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return SqlRoleEntityRepository::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabCriteriaIdFromId($tabId)
    {
        // Init var
        $result = array();

        if(count($tabId) > 0)
        {
            // Init var
            $result = null;
            $objRepository = $this->getObjRepository();
            /** @var SqlRolePermEntityCollectionRepository $objRolePermEntityCollectionRepo */
            $objRolePermEntityCollectionRepo = $this->objRolePermEntityCollectionRepo;
            $objConnection = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjConnection();
            $objSelectCommand = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjSelectCommand();

            // Get SQL arguments
            $strTableNm = $objRepository->getStrSqlTableName();
            $strRolePermTableNm = $objRolePermEntityCollectionRepo->getObjRepository()->getStrSqlTableName();

            $tabSqlId = array_map(
                function($intId) use ($objConnection) {
                    return $objConnection->getStrEscapeValue($intId);
                },
                $tabId
            );
            $strTabSqlId = implode(', ', $tabSqlId);

            // Build select command
            $objSelectCommand
                ->setClauseSelect(
                    array(
                        ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID,
                        ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID
                    )
                )
                ->setClauseFrom(
                    array(
                        [
                            'type' => 'left_outer',
                            'left' => $strTableNm,
                            'right' => $strRolePermTableNm,
                            'on' => [
                                'content' => [
                                    [
                                        'operand' => [
                                            'table_name' => $strTableNm,
                                            'column_name' => ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID
                                        ],
                                        'value' => [
                                            'table_name' => $strRolePermTableNm,
                                            'column_name' => ConstRolePerm::ATTRIBUTE_NAME_SAVE_PERM_ROLE_ID
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    )
                )
                ->setClauseWhere(
                    array(
                        'content' => [
                            [
                                'operand' => ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID,
                                'operator' => 'in',
                                'value' => ['pattern' => $strTabSqlId]
                            ]
                        ]
                    )
                )
                ->setClauseOrder(
                    array(
                        [
                            'operand' => [
                                'pattern' => sprintf(
                                    'FIELD(%1$s, %2$s)',
                                    $objConnection->getStrEscapeName(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID),
                                    $strTabSqlId
                                )
                            ],
                            'operator' => 'asc'
                        ],
                        [
                            'operand' => ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID,
                            'value' => 'asc'
                        ]
                    )
                );

            // Execute query
            $objDbResult = $objSelectCommand->executeResult();
            if($objDbResult !== false)
            {
                // Get criteria, if found
                $tabData = $objDbResult->getTabData();
                $result = ToolBoxSubRepo::getTabCriteriaFromData($tabData, $objRepository);

                // Close query
                $objDbResult->close();
            }
        }

        // Return result
        return $result;
    }





    // Methods subject getters
    // ******************************************************************************

    /**
     * Get configuration array,
     * for subject role persistence.
     *
     * @return array
     */
    protected function getTabSubjectRoleRefPersistorConfig()
    {
        // Init var
        $strSubjectRoleRefTableNm = $this->getObjRepository()->getStrSqlSubjectRoleRefTableName();

        // Return result
        return array(
            ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME => $strSubjectRoleRefTableNm,
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID  => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ID,
        );
    }



    /**
     * Get index array of subject role data,
     * from specified role subjects.
     *
     * @param RoleSubjectInterface[] $tabRoleSubject
     * @return null|array
     */
    protected function getTabSubjectRoleRefData(array $tabRoleSubject)
    {
        // Init var
        $tabRoleSubject = array_values($tabRoleSubject);

        // Run each role subject
        $boolValid = true;
        $tabSubjRoleData = array();
        for($intCpt = 0; ($intCpt < count($tabRoleSubject)) && $boolValid; $intCpt++)
        {
            // Check valid role collection
            $objRoleSubject = $tabRoleSubject[$intCpt];
            $objRoleEntityCollection = $objRoleSubject->getObjRoleCollection();
            $boolValid = ($objRoleEntityCollection instanceof RoleEntityCollection);

            if($boolValid)
            {
                /** @var RoleEntityCollection $objRoleEntityCollection */

                // Run each role
                foreach($objRoleEntityCollection->getTabKey() as $strKey)
                {
                    $objRoleEntity = $objRoleEntityCollection->getItem($strKey);

                    // Set data
                    $tabSubjRoleData[] = array(
                        ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID =>
                            $objRoleEntity->getAttributeValueSave(ConstRole::ATTRIBUTE_KEY_ID),
                        ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE =>
                            $objRoleSubject->getStrSubjectType(),
                        ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID =>
                            $objRoleSubject->getStrSubjectId()
                    );
                }
            }
        }

        // Set result
        $result = ($boolValid ? $tabSubjRoleData : null);

        // Return result
        return $result;
    }



    /**
     * Get role subject key (can be used as identifier),
     * from specified subject type,
     * and subject id.
     *
     * @param string $strSubjectType
     * @param string $strSubjectId
     * @return string
     */
    protected function getStrSubjectKey($strSubjectType, $strSubjectId)
    {
        // Return result
        return sprintf(
            '%1$s_%2$s',
            $strSubjectType,
            $strSubjectId
        );
    }



    /**
     * Get array of role ids, per role subject,
     * from specified role subjects.
     *
     * Array return format:
     * [
     *     // Index array of role ids, for role subject 1
     *     'string role subject type' . '-' . 'string role subject id' => [
     *         integer role id 1,
     *         ...,
     *         integer role id N
     *     ],
     *
     *     ...,
     *
     *     // Index array of role ids, for role subject N
     *     [...]
     * ]
     *
     * @param RoleSubjectInterface[] $tabRoleSubject
     * @return null|array
     */
    protected function getTabIdFromSubject(array $tabRoleSubject)
    {
        // Init var
        $result = array();

        if(count($tabRoleSubject) > 0)
        {
            // Init var
            $result = null;
            $objConnection = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjConnection();
            $objSelectCommand = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjSelectCommand();

            // Get SQL arguments
            $strSubjectRoleRefTableNm = $this->getObjRepository()->getStrSqlSubjectRoleRefTableName();
            $tabSqlType = array_map(
                function($objRoleSubject) use ($objConnection) {
                    /** @var RoleSubjectInterface $objRoleSubject */
                    return $objConnection->getStrEscapeValue($objRoleSubject->getStrSubjectType());
                },
                $tabRoleSubject
            );
            $tabSqlType = array_unique($tabSqlType);
            $strTabSqlType = implode(', ', $tabSqlType);

            $tabSqlId = array_map(
                function($objRoleSubject) use ($objConnection) {
                    /** @var RoleSubjectInterface $objRoleSubject */
                    return $objConnection->getStrEscapeValue($objRoleSubject->getStrSubjectId());
                },
                $tabRoleSubject
            );
            $strTabSqlId = implode(', ', $tabSqlId);

            // Build select command
            $objSelectCommand
                ->setClauseSelect(
                    array(
                        ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE,
                        ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID,
                        ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID
                    )
                )
                ->setClauseFrom(array($strSubjectRoleRefTableNm))
                ->setClauseWhere(
                    array(
                        'content' => [
                            [
                                'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE,
                                'operator' => 'in',
                                'value' => ['pattern' => $strTabSqlType]
                            ],
                            [
                                'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID,
                                'operator' => 'in',
                                'value' => ['pattern' => $strTabSqlId]
                            ]
                        ]
                    )
                );

            // Execute query
            $objDbResult = $objSelectCommand->executeResult();

            // Run query result
            if($objDbResult !== false)
            {
                $result = array();
                while(($data = $objDbResult->getFetchData()) !== false)
                {
                    // Get data
                    $strSubjectKey = $this->getStrSubjectKey(
                        $data[ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE],
                        $data[ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID]
                    );
                    $intRoleId = intval($data[ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID]);

                    // Init result, if required
                    if (!array_key_exists($strSubjectKey, $result)) $result[$strSubjectKey] = array();

                    // Set data in result
                    $result[$strSubjectKey][] = $intRoleId;
                }

                // Close query
                $objDbResult->close();
            }
        }

        // Return result
        return $result;
    }





    // Methods subject repository
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws TabSubjectInvalidFormatException
     */
    public function loadSubjectRole($roleSubject)
    {
        // Init var
        $tabRoleSubject = (
            is_array($roleSubject)?
                array_values($roleSubject) :
                array($roleSubject)
        );

        // Set check argument
        TabSubjectInvalidFormatException::setCheck($tabRoleSubject);

        // Init var
        $result = false;
        $tabIdPerSubj = $this->getTabIdFromSubject($tabRoleSubject);
        $objRoleEntityCollection = $this->objRoleEntityCollection;

        // Init global role entity collection, if required
        if(!is_null($objRoleEntityCollection))
        {
            $tabRoleId = array();
            foreach($tabIdPerSubj as $strSubjectKey => $tabId)
            {
                foreach($tabId as $intId)
                {
                    if(!in_array($intId, $tabRoleId))
                    {
                        $tabRoleId[] = $intId;
                    }
                }
            }

            $objRoleEntityCollection->removeItemAll();
            $result = $this->loadFromId($objRoleEntityCollection, $tabRoleId);
        }

        // Run each role subject
        foreach($tabRoleSubject as $objRoleSubject)
        {
            /** @var RoleSubjectInterface $objRoleSubject */

            // Check valid role collection
            $objSubjRoleEntityCollection = $objRoleSubject->getObjRoleCollection();
            if($objSubjRoleEntityCollection instanceof RoleEntityCollection)
            {
                // Get index array of role ids
                $strSubjectKey = $this->getStrSubjectKey(
                    $objRoleSubject->getStrSubjectType(),
                    $objRoleSubject->getStrSubjectId()
                );

                // Check roles found, for role subject
                if(array_key_exists($strSubjectKey, $tabIdPerSubj))
                {
                    $tabId = $tabIdPerSubj[$strSubjectKey];

                    // Get roles from global collection, if required
                    if(!is_null($objRoleEntityCollection))
                    {
                        if($result)
                        {
                            $tabRoleEntity = array();
                            foreach($objRoleEntityCollection->getTabKey() as $strKey)
                            {
                                $objRoleEntity = $objRoleEntityCollection->getItem($strKey);
                                if(in_array($objRoleEntity->getAttributeValue(ConstRole::ATTRIBUTE_KEY_ID), $tabId))
                                {
                                    $tabRoleEntity[] = $objRoleEntity;
                                }
                            }
                            $objSubjRoleEntityCollection->setTabRole($tabRoleEntity);
                        }
                    }
                    // Else: load role collection
                    else
                    {
                        $result = $this->loadFromId($objSubjRoleEntityCollection, $tabId) && $result;
                    }
                }
            }
        }

        // Reset global role entity collection, if required
        if(!is_null($objRoleEntityCollection))
        {
            $objRoleEntityCollection->removeItemAll();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabSubjectInvalidFormatException
     */
    public function saveSubjectRoleRef($roleSubject)
    {
        // Init var
        $tabRoleSubject = (
            is_array($roleSubject)?
                array_values($roleSubject) :
                array($roleSubject)
        );

        // Set check argument
        TabSubjectInvalidFormatException::setCheck($tabRoleSubject);

        // Init var
        $result = false;
        $objPersistor = $this->getObjPersistor();
        $tabConfig = $this->getTabSubjectRoleRefPersistorConfig();
        $tabData = $this->getTabSubjectRoleRefData($tabRoleSubject);

        // Save subject role data, if required
        if(!is_null($tabData))
        {
            // Remove all previous subject role data
            $result = $this->removeSubjectRoleRef($tabRoleSubject, true);

            // Save subject role data
            $result = $result && $objPersistor->createTabData($tabData, $tabConfig);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabSubjectInvalidFormatException
     */
    public function removeSubjectRoleRef($roleSubject, $boolAll = true)
    {
        // Init var
        $tabRoleSubject = (
            is_array($roleSubject)?
                array_values($roleSubject) :
                array($roleSubject)
        );

        // Set check argument
        TabSubjectInvalidFormatException::setCheck($tabRoleSubject);

        // Init var
        $result = true;
        $boolAll = (is_bool($boolAll) ? $boolAll : true);

        if(count($tabRoleSubject) > 0)
        {
            // Init var
            $objDeleteCommand = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjDeleteCommand();

            // Get SQL arguments
            $strSubjectRoleRefTableNm = $this->getObjRepository()->getStrSqlSubjectRoleRefTableName();

            $tabSqlConfigWhere = null;
            if($boolAll)
            {
                $tabSqlConfigWhere = array_map(
                    function($objRoleSubject) {
                        /** @var RoleSubjectInterface $objRoleSubject */
                        return array(
                            'content' => [
                                [
                                    'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE,
                                    'operator' => 'equal',
                                    'value' => ['value' => $objRoleSubject->getStrSubjectType()]
                                ],
                                [
                                    'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID,
                                    'operator' => 'equal',
                                    'value' => ['value' => $objRoleSubject->getStrSubjectId()]
                                ]
                            ]
                        );
                    },
                    $tabRoleSubject
                );
            }
            else
            {
                $tabData = $this->getTabSubjectRoleRefData($tabRoleSubject);
                if(!is_null($tabData))
                {
                    $tabSqlConfigWhere = array_map(
                        function(array $data) {
                            return array(
                                'content' => [
                                    [
                                        'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID,
                                        'operator' => 'equal',
                                        'value' => ['value' => $data[ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID]]
                                    ],
                                    [
                                        'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE,
                                        'operator' => 'equal',
                                        'value' => ['value' => $data[ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE]]
                                    ],
                                    [
                                        'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID,
                                        'operator' => 'equal',
                                        'value' => ['value' => $data[ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID]]
                                    ]
                                ]
                            );
                        },
                        $tabData
                    );
                }
            }

            // Build delete command, if required
            $result = false;
            if(!is_null($tabSqlConfigWhere))
            {
                $objDeleteCommand
                    ->setClauseFrom(array($strSubjectRoleRefTableNm))
                    ->setClauseWhere(
                        array(
                            'type' => 'or',
                            'content' => $tabSqlConfigWhere
                        )
                    );

                // Execute query
                $result = ($objDeleteCommand->execute() !== false);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeSubjectRoleRefFromRole(RoleEntityCollection $objRoleEntityCollection)
    {
        // Init var
        $result = true;
        $tabRoleEntity = $objRoleEntityCollection->getTabItem();

        if(count($tabRoleEntity) > 0)
        {
            // Init var
            $objConnection = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                -> getObjConnection();
            $objDeleteCommand = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjDeleteCommand();

            // Get SQL arguments
            $strSubjectRoleRefTableNm = $this->getObjRepository()->getStrSqlSubjectRoleRefTableName();

            $tabSqlId = array_map(
                function($objRoleEntity) use ($objConnection) {
                    /** @var RoleEntity $objRoleEntity */
                    return $objConnection->getStrEscapeValue($objRoleEntity->getAttributeValue(ConstRole::ATTRIBUTE_KEY_ID));
                },
                $tabRoleEntity
            );
            $strTabSqlId = implode(', ', $tabSqlId);

            // Build delete command, if required
            $objDeleteCommand
                ->setClauseFrom(array($strSubjectRoleRefTableNm))
                ->setClauseWhere(
                    array(
                        'content' => [
                            [
                                'operand' => ConstSqlRole::SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID,
                                'operator' => 'in',
                                'value' => ['pattern' => $strTabSqlId]
                            ]
                        ]
                    )
                );

            // Execute query
            $result = ($objDeleteCommand->execute() !== false);
        }

        // Return result
        return $result;
    }



}