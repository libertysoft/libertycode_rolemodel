<?php
/**
 * This class allows to define SQL role entity class.
 * SQL role entity allows to design a role entity class,
 * using SQL rules for attributes validation.
 *
 * SQL role entity uses the following specified configuration:
 * [
 *     Role entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\sql\model;

use liberty_code\role_model\role\model\RoleEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityRepository;



class SqlRoleEntity extends RoleEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * SQL role entity repository instance.
     * @var null|SqlRoleEntityRepository
     */
    protected $objRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlRoleEntityRepository $objRepository = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        SqlRoleEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objDateTimeFactory
        );

        // Init SQL role entity repository
        $this->setRepository($objRepository);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = parent::getTabRuleConfig();
        $objRepository = $this->getObjRepository();

        // Get rule configurations, if required
        if(!is_null($objRepository))
        {
            $result = array_merge(
                $result,
                array(
                    ConstRole::ATTRIBUTE_KEY_ID => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-null' => [
                                        'is_null',
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return $this->checkIsNew();}
                                            ]
                                        ]
                                    ],
                                    'is-valid-id' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return (!$this->checkIsNew());}
                                            ]
                                        ],
                                        [
                                            'sql_exist',
                                            [
                                                'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                                'table_name' => $objRepository->getStrSqlTableName(),
                                                'column_name' => $this->getAttributeNameSave(ConstRole::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                            ]
                        ]
                    ],
                    ConstRole::ATTRIBUTE_KEY_NAME => [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'sql_exist',
                                        [
                                            'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                            'table_name' => $objRepository->getStrSqlTableName(),
                                            'column_name' => $this->getAttributeNameSave(ConstRole::ATTRIBUTE_KEY_NAME),
                                            'exclude' => [
                                                $this->getAttributeNameSave(ConstRole::ATTRIBUTE_KEY_ID) =>
                                                    $this->getAttributeValueSave(ConstRole::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be unique.'
                            ]
                        ]
                    ]
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get SQL role entity repository object.
     *
     * @return null|SqlRoleEntityRepository
     */
    public function getObjRepository()
    {
        // Return result
        return $this->objRepository;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set SQL role entity repository object.
     *
     * @param SqlRoleEntityRepository $objRepository = null
     */
    public function setRepository(SqlRoleEntityRepository $objRepository = null)
    {
        $this->objRepository = $objRepository;
    }



}