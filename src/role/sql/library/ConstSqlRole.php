<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\sql\library;



class ConstSqlRole
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_TABLE_NAME_ROLE = 'role';

    const SQL_TABLE_NAME_ROLE_SUBJ = 'role_subject';
    const SQL_COLUMN_NAME_ROLE_SUBJ_ID = 'role_subj_id';
    const SQL_COLUMN_NAME_ROLE_SUBJ_ROLE_ID = 'role_subj_role_id';
    const SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_TYPE = 'role_subj_subj_type';
    const SQL_COLUMN_NAME_ROLE_SUBJ_SUBJ_ID = 'role_subj_subj_id';
}