<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\exception;

use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\subject\api\RoleSubjectInterface;



class TabSubjectInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $subject
     */
	public function __construct($subject)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRole::EXCEPT_MSG_TAB_SUBJECT_INVALID_FORMAT,
            mb_strimwidth(strval($subject), 0, 50, "...")
        );
	}





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified subject has valid format.
     *
     * @param mixed $subject
     * @return boolean
     * @throws static
     */
    static public function setCheck($subject)
    {
        // Init var
        $result =
            // Check valid array
            is_array($subject);

        // Run each subject
        $subject = ($result ? array_values($subject) : $subject);
        for($intCpt = 0; $result && ($intCpt < count($subject)); $intCpt++)
        {
            $objSubject = $subject[$intCpt];

            // Check valid subject
            $result =
                is_object($objSubject) &&
                ($objSubject instanceof RoleSubjectInterface);
        }

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($subject) ? serialize($subject) : $subject));
        }

        // Return result
        return $result;
    }
	
	
	
}