<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/role/permission/test/RolePermRepositoryTest.php');

// Use
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\permission\model\RolePermEntityCollection;



// Init var
$objRolePermEntityCollection = new RolePermEntityCollection();



// Test hydrate role permission entity collection
echo('Test hydrate role permission entity collection: <br />');

echo('Before hydrate: <pre>');
var_dump(getTabRolePermEntityCollectionData($objRolePermEntityCollection));
echo('</pre>');

$objRolePermSpecBuilder->hydratePermissionCollection($objRolePermEntityCollection);

echo('After hydrate: <pre>');
var_dump(getTabRolePermEntityCollectionData($objRolePermEntityCollection));
echo('</pre>');

echo('<br /><br /><br />');



// Test get
echo('Test get role permission: <br />');

try
{
    $objRolePermEntityCollection->setPermissionAllEnable('test');
}
catch (Exception $e)
{
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}
$objRolePermEntityCollection->setPermissionAllEnable(true);

foreach($objRolePermEntityCollection->getTabPermissionKey() as $strPermissionKey)
{
    echo('Test get role permission key "'.$strPermissionKey.'": <br />');

    $objRolePermEntity = $objRolePermEntityCollection->getObjPermission($strPermissionKey);

    echo('Get role permission key: <pre>');var_dump($objRolePermEntity->getStrPermissionKey());echo('</pre>');
    echo('Check role permission enable: <pre>');var_dump($objRolePermEntity->checkPermissionEnable());echo('</pre>');
    echo('Get role permission configuration: <pre>');var_dump($objRolePermEntity->getTabPermissionConfig());echo('</pre>');
    echo('Check collection role permission enable: <pre>');var_dump($objRolePermEntityCollection->checkPermissionEnable($strPermissionKey));echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test re-hydrate role permission entity collection
echo('Test re-hydrate role permission entity collection: <br />');

$tabRolePermEntity = array(
    $objRolePermEntityFactory->getObjPermission(array(
        ConstPermission::ATTRIBUTE_KEY_KEY => 'test-add-1',
        ConstPermission::ATTRIBUTE_KEY_VALUE => true
    )),
    $objRolePermEntityFactory->getObjPermission(array(
        ConstPermission::ATTRIBUTE_KEY_KEY => 'test-add-2'
    )),
    $objRolePermEntityFactory->getObjPermission(array(
        ConstPermission::ATTRIBUTE_KEY_KEY => 'test-add-3',
        ConstPermission::ATTRIBUTE_KEY_VALUE => true
    ))
);
$objRolePermEntityCollection->setTabPermission($tabRolePermEntity);

echo('Before hydrate: <pre>');
var_dump(getTabRolePermEntityCollectionData($objRolePermEntityCollection));
echo('</pre>');

$objRolePermSpecBuilder->hydratePermissionCollection($objRolePermEntityCollection, false);

echo('After hydrate: <pre>');
var_dump(getTabRolePermEntityCollectionData($objRolePermEntityCollection));
echo('</pre>');

echo('<br /><br /><br />');


