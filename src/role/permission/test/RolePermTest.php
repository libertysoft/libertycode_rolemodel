<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load external library test
require_once($strRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Load test
require_once($strRootAppPath . '/vendor/liberty_code/role/src/permission/specification/test/PermSpecTest.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\model\validation\rule\entity_collection\attribute_exist\model\AttrExistEntityCollectionRule;
use liberty_code\model\validation\rule\validation_entity\model\ValidEntityRule;
use liberty_code\model\validation\rule\validation_entity_collection\model\ValidEntityCollectionRule;
use liberty_code\model\validation\rule\new_entity\model\NewEntityRule;
use liberty_code\model\validation\rule\new_entity_collection\model\NewEntityCollectionRule;
use liberty_code\sql\validation\rule\sql_exist\model\ExistSqlRule;
use liberty_code\role\permission\build\model\DefaultBuilder;
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;
use liberty_code\role\permission\specification\test\PermSpecTest;
use liberty_code\role_model\role\permission\model\RolePermEntity;
use liberty_code\role_model\role\permission\model\RolePermEntityCollection;
use liberty_code\role_model\role\permission\sql\model\SqlRolePermEntityFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);



// Init validator
$objPref = new Preference(array(
    'source' => ValidatorInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objValidator],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Add rule
$objAttrExistEntityCollectionRule = new AttrExistEntityCollectionRule();
$objValidEntityRule = new ValidEntityRule();
$objValidEntityCollectionRule = new ValidEntityCollectionRule();
$objNewEntityRule = new NewEntityRule();
$objNewEntityCollectionRule = new NewEntityCollectionRule();
$objExistSqlRule = new ExistSqlRule();

$tabRule = array(
    $objAttrExistEntityCollectionRule,
    $objValidEntityRule,
    $objValidEntityCollectionRule,
    $objNewEntityRule,
    $objNewEntityCollectionRule,
    $objExistSqlRule
);
$objRuleCollection->setTabRule($tabRule);



// Init datetime factory
$tabConfig = array(
    // ConstDateTimeFactory::TAB_CONFIG_KEY_ENTITY_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME => 'Europe/Paris',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT => 'Y-m-d H:i:s.u',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME => 'America/New_York',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT => 'Y-m-d H:i:s.u',
    // ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT => 'Y-m-d H:i:s'
);
$objDateTimeFactory = new DefaultDateTimeFactory($tabConfig);

$objPref = new Preference(array(
    'source' => DateTimeFactoryInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objDateTimeFactory],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init permission specification
$objPermSpec = new PermSpecTest();



// Init factory
$objRolePermEntityFactory = new SqlRolePermEntityFactory($objProvider);



// Init builder
$objRolePermBuilder = new DefaultBuilder($objRolePermEntityFactory);

$objRolePermSpecBuilder = new PermSpecBuilder(
    $objPermSpec,
    $objRolePermEntityFactory
);



// Init function
function getTabRolePermEntityData(RolePermEntity $objRolePermEntity)
{
    return $objRolePermEntity->getTabData();
}



function getTabRolePermEntityCollectionData(RolePermEntityCollection $objRolePermEntityCollection)
{
    $result = array();

    foreach($objRolePermEntityCollection->getTabKey() as $strKey)
    {
        $objRolePermEntity = $objRolePermEntityCollection->getItem($strKey);
        $result[$strKey] = getTabRolePermEntityData($objRolePermEntity);
    }

    return $result;
}


