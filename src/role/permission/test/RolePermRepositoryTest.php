<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/db/HelpCreateDbTest.php');
require_once($strRootAppPath . '/src/role/permission/test/RolePermTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;

use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityRepository;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityCollectionRepository;



// Init repository
$objRolePermEntityRepo = new SqlRolePermEntityRepository($objTablePersistor);

$objPref = new Preference(array(
    'source' => SqlRolePermEntityRepository::class,
    'set' =>  ['type' => 'instance', 'value' => $objRolePermEntityRepo],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

// Init collection repository
$objRolePermEntityCollectionRepo = new SqlRolePermEntityCollectionRepository(
    $objRolePermEntityFactory,
    $objRolePermEntityRepo
);


