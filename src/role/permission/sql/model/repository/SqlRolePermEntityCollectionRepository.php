<?php
/**
 * This class allows to define SQL role permission entity collection repository class.
 * SQL role permission entity collection repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\sql\model\repository;

use liberty_code\role_model\role\permission\model\repository\RolePermEntityCollectionRepository;

use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\role_model\role\permission\model\RolePermEntityFactory;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityRepository;



/**
 * @method null|SqlRolePermEntityRepository getObjRepository() @inheritdoc
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlRolePermEntityCollectionRepository extends RolePermEntityCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlRolePermEntityRepository $objRepository = null
     */
    public function __construct(
        RolePermEntityFactory $objEntityFactory = null,
        SqlRolePermEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return SqlRolePermEntityRepository::class;
    }



}