<?php
/**
 * This class allows to define SQL role permission entity repository class.
 * SQL role permission entity repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\sql\model\repository;

use liberty_code\role_model\role\permission\model\repository\RolePermEntityRepository;

use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\permission\library\ConstRolePerm;
use liberty_code\role_model\role\permission\sql\library\ConstSqlRolePerm;



/**
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlRolePermEntityRepository extends RolePermEntityRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TablePersistor $objPersistor = null
     */
    public function __construct(TablePersistor $objPersistor = null)
    {
        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return TablePersistor::class;
    }



    /**
     * Get SQL table name where role permissions stored.
     * Overwrite it to implement specific table name.
     *
     * @return string
     */
    public function getStrSqlTableName()
    {
        // Return result
        return ConstSqlRolePerm::SQL_TABLE_NAME_ROLE_PERM;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array(
            ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME => $this->getStrSqlTableName(),
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID  => ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID,
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set role permission structure.
     * Return true if correctly run, false else.
     *
     * @return boolean
     */
    public function setStructure()
    {
        // Init var
        $objConnection = $this
            ->getObjPersistor()
            ->getObjCommandFactory()
            ->getObjConnection();

        // Create role permission table
        $strSql = sprintf(
            'CREATE TABLE %1$s (
            %2$s int(10) unsigned NOT NULL AUTO_INCREMENT,
            %3$s datetime NOT NULL,
            %4$s datetime NOT NULL,
            %5$s varchar(100) NOT NULL,
            %6$s tinyint(1) NOT NULL,
            %7$s int(10) unsigned NOT NULL,
            PRIMARY KEY (%2$s)
        );',
            $objConnection->getStrEscapeName($this->getStrSqlTableName()),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_DT_CREATE),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_DT_UPDATE),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_KEY),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_VALUE),
            $objConnection->getStrEscapeName(ConstRolePerm::ATTRIBUTE_NAME_SAVE_PERM_ROLE_ID)
        );
        $result = ($objConnection->execute($strSql) !== false);

        // Return result
        return $result;
    }



}


