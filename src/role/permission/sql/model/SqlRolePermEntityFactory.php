<?php
/**
 * This class allows to define SQL role permission entity factory class.
 * SQL role permission entity factory is role permission entity factory,
 * to provide new SQL role permission entities.
 *
 * SQL role permission entity factory uses the following specified configuration, to get and hydrate role permission:
 * [
 *     SQL role permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\sql\model;

use liberty_code\role_model\role\permission\model\RolePermEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role_model\role\permission\sql\model\SqlRolePermEntity;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityRepository;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityRepository;



/**
 * @method SqlRolePermEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|SqlRolePermEntity getObjPermission(array $tabConfig = array(), string $strConfigKey = null, PermissionInterface $objPermission = null) @inheritdoc
 */
class SqlRolePermEntityFactory extends RolePermEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SqlRolePermEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SqlRolePermEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objRepository = $this->getObjInstance(SqlRolePermEntityRepository::class);
        $objRoleEntityRepository = $this->getObjInstance(SqlRoleEntityRepository::class);
        $result = new SqlRolePermEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objRepository,
            $objRoleEntityRepository
        );

        // Return result
        return $result;
    }



}