<?php
/**
 * This class allows to define SQL role permission entity class.
 * SQL role permission entity allows to design a role permission entity class,
 * using SQL rules for attributes validation.
 *
 * SQL role permission entity uses the following specified configuration:
 * [
 *     Role permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\sql\model;

use liberty_code\role_model\role\permission\model\RolePermEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\permission\library\ConstRolePerm;
use liberty_code\role_model\role\permission\sql\model\repository\SqlRolePermEntityRepository;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityRepository;



class SqlRolePermEntity extends RolePermEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * SQL role permission entity repository instance.
     * @var null|SqlRolePermEntityRepository
     */
    protected $objRepository;



    /**
     * SQL role entity repository instance.
     * @var null|SqlRoleEntityRepository
     */
    protected $objRoleEntityRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlRolePermEntityRepository $objRepository = null
     * @param SqlRoleEntityRepository $objRoleEntityRepository = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        SqlRolePermEntityRepository $objRepository = null,
        SqlRoleEntityRepository $objRoleEntityRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objDateTimeFactory
        );

        // Init SQL role permission entity repository
        $this->setRepository($objRepository);

        // Init SQL role entity repository
        $this->setRoleEntityRepository($objRoleEntityRepository);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = parent::getTabRuleConfig();
        $objRepository = $this->getObjRepository();
        $objRoleEntityRepository = $this->getObjRoleEntityRepository();

        // Get rule configurations, if required
        if(!is_null($objRepository))
        {
            $result = array_merge(
                $result,
                array(
                    ConstPermission::ATTRIBUTE_KEY_ID => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-null' => [
                                        'is_null',
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return $this->checkIsNew();}
                                            ]
                                        ]
                                    ],
                                    'is-valid-id' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return (!$this->checkIsNew());}
                                            ]
                                        ],
                                        [
                                            'sql_exist',
                                            [
                                                'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                                'table_name' => $objRepository->getStrSqlTableName(),
                                                'column_name' => $this->getAttributeNameSave(ConstPermission::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                            ]
                        ]
                    ],
                    ConstPermission::ATTRIBUTE_KEY_KEY => [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'sql_exist',
                                        [
                                            'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                            'table_name' => $objRepository->getStrSqlTableName(),
                                            'column_name' => $this->getAttributeNameSave(ConstPermission::ATTRIBUTE_KEY_KEY),
                                            'include' => [
                                                $this->getAttributeNameSave(ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID) =>
                                                    $this->getAttributeValueSave(ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID)
                                            ],
                                            'exclude' => [
                                                $this->getAttributeNameSave(ConstPermission::ATTRIBUTE_KEY_ID) =>
                                                    $this->getAttributeValueSave(ConstPermission::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be unique.'
                            ]
                        ]
                    ]
                )
            );
        }

        // Get rule configurations, if required
        if(!is_null($objRoleEntityRepository))
        {
            $result = array_merge(
                $result,
                array(
                    ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-null' => ['is_null'],
                                    'is-valid-id' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'sql_exist',
                                            [
                                                'command_factory' => $objRoleEntityRepository->getObjPersistor()->getObjCommandFactory(),
                                                'table_name' => $objRoleEntityRepository->getStrSqlTableName(),
                                                'column_name' => ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be null or a valid role ID.'
                            ]
                        ]
                    ]
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get SQL role permission entity repository object.
     *
     * @return null|SqlRolePermEntityRepository
     */
    public function getObjRepository()
    {
        // Return result
        return $this->objRepository;
    }



    /**
     * Get SQL role entity repository object.
     *
     * @return null|SqlRoleEntityRepository
     */
    public function getObjRoleEntityRepository()
    {
        // Return result
        return $this->objRoleEntityRepository;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set SQL role permission entity repository object.
     *
     * @param SqlRolePermEntityRepository $objRepository = null
     */
    public function setRepository(SqlRolePermEntityRepository $objRepository = null)
    {
        $this->objRepository = $objRepository;
    }



    /**
     * Set SQL role entity repository object.
     *
     * @param SqlRoleEntityRepository $objRepository = null
     */
    public function setRoleEntityRepository(SqlRoleEntityRepository $objRepository = null)
    {
        $this->objRoleEntityRepository = $objRepository;
    }



}