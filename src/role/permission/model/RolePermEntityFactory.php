<?php
/**
 * This class allows to define role permission entity factory class.
 * Role permission entity factory is permission entity factory,
 * to provide new role permission entities.
 *
 * Role permission entity factory uses the following specified configuration, to get and hydrate role permission:
 * [
 *     Role permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\model;

use liberty_code\role_model\permission\model\PermissionEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role_model\role\permission\model\RolePermEntity;



/**
 * @method RolePermEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|RolePermEntity getObjPermission(array $tabConfig = array(), string $strConfigKey = null, PermissionInterface $objPermission = null) @inheritdoc
 */
class RolePermEntityFactory extends PermissionEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RolePermEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return RolePermEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new RolePermEntity(
            array(),
            $objValidator,
            $objDateTimeFactory
        );

        // Return result
        return $result;
    }



}