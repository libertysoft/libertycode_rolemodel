<?php
/**
 * This class allows to define role permission entity class.
 * Role permission entity is permission entity,
 * related to a specific role.
 *
 * Role permission entity uses the following specified configuration:
 * [
 *     Permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\model;

use liberty_code\role_model\permission\model\PermissionEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\role_model\role\permission\library\ConstRolePerm;



/**
 * @property null|integer $intAttrRoleId
 */
class RolePermEntity extends PermissionEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute role id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRolePerm::ATTRIBUTE_ALIAS_ROLE_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstRolePerm::ATTRIBUTE_NAME_SAVE_PERM_ROLE_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-integer' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                        ]
                    ]
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID:
                $result = strval($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID:
                $result = intval($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}