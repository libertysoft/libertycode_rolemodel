<?php
/**
 * This class allows to define role permission entity collection repository class.
 * Role permission entity collection repository allows to prepare data from role permission entity collection,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\permission\model\RolePermEntityFactory;
use liberty_code\role_model\role\permission\model\repository\RolePermEntityRepository;



/**
 * @method null|RolePermEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|RolePermEntityRepository getObjRepository() @inheritdoc
 */
class RolePermEntityCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RolePermEntityFactory $objEntityFactory = null
     * @param RolePermEntityRepository $objRepository = null
     */
    public function __construct(
        RolePermEntityFactory $objEntityFactory = null,
        RolePermEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return RolePermEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return RolePermEntityRepository::class;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Update datetime create, update, on entities
        ToolBoxDateTime::hydrateEntityCollectionAttrDtCreateUpdate(
            $objCollection,
            ConstPermission::ATTRIBUTE_KEY_DT_CREATE,
            ConstPermission::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objCollection, $tabConfig, $tabInfo);
    }



}