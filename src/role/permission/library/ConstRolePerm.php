<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\permission\library;



class ConstRolePerm
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ROLE_ID = 'intAttrRoleId';

    const ATTRIBUTE_ALIAS_ROLE_ID = 'role-id';

    const ATTRIBUTE_NAME_SAVE_PERM_ROLE_ID = 'perm_role_id';
}