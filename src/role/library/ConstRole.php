<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\library;



class ConstRole
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION = 'objAttrRolePermEntityCollection';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_ROLE_PERM_ENTITY_COLLECTION = 'permission';

    const ATTRIBUTE_NAME_SAVE_ROLE_ID = 'role_id';
    const ATTRIBUTE_NAME_SAVE_ROLE_DT_CREATE = 'role_dt_create';
    const ATTRIBUTE_NAME_SAVE_ROLE_DT_UPDATE = 'role_dt_update';
    const ATTRIBUTE_NAME_SAVE_ROLE_NM = 'role_nm';



    // Exception message constants
    const EXCEPT_MSG_NAME_NOT_FOUND = 'Following role name "%1$s" not found in role collection.';
    const EXCEPT_MSG_TAB_SUBJECT_INVALID_FORMAT =
        'Following array subjects "%1$s" invalid! It must be an index array of role subject objects.';
}