<?php
/**
 * Description :
 * This class allows to describe behavior of role subject class.
 * Role subject is role subject item, can be used in role entities.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\subject\api;

use liberty_code\role\role\subject\api\RoleSubjectInterface as BaseRoleSubjectInterface;
use liberty_code\role_model\subject\api\SubjectInterface;



interface RoleSubjectInterface extends BaseRoleSubjectInterface, SubjectInterface
{

}