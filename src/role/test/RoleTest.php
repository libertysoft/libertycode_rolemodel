<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/role/permission/test/RolePermTest.php');

// Use
use liberty_code\role\role\build\model\DefaultBuilder;
use liberty_code\role_model\role\permission\model\RolePermEntityCollection;
use liberty_code\role_model\role\model\RoleEntity;
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\sql\model\SqlRoleEntityFactory;



// Init factory
$objRoleEntityFactory = new SqlRoleEntityFactory($objProvider);



// Init builder
$objRoleBuilder = new DefaultBuilder(
    $objRoleEntityFactory,
    array(
        $objRolePermBuilder,
        $objRolePermSpecBuilder
    )
);



// Init function
function getTabRoleEntityData(RoleEntity $objRoleEntity)
{
    $result = array();

    foreach($objRoleEntity->getTabAttributeKey() as $strAttrKey)
    {
        $value = $objRoleEntity->getAttributeValue($strAttrKey);
        if($strAttrKey == 'objAttrRolePermEntityCollection')
        {
            $result[$strAttrKey] = null;
            if($value instanceof RolePermEntityCollection)
            {
                $result[$strAttrKey] = getTabRolePermEntityCollectionData($value);
            }
        }
        else
        {
            $result[$strAttrKey] = $value;
        }
    }

    return $result;
}



function getTabRoleEntityCollectionData(RoleEntityCollection $objRoleEntityCollection)
{
    $result = array();

    foreach($objRoleEntityCollection->getTabKey() as $strKey)
    {
        $objRoleEntity = $objRoleEntityCollection->getItem($strKey);
        $result[$strKey] = getTabRoleEntityData($objRoleEntity);
    }

    return $result;
}


