<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/role/test/RoleRepositoryTest.php');

// Use
use liberty_code\role_model\role\model\RoleEntityCollection;



// Init var
$objRoleEntityCollection = new RoleEntityCollection();

$tabDataSrc = array(
    'role-1' => [
        'permission_data_src' => [
            'test_get' => [
                'boolAttrValue' => false
            ],
            'test_set' => [
                'boolAttrValue' => false
            ],
            'test_delete' => [
                'boolAttrValue' => true
            ]
        ],
        'key-test-1' => 'Value test 1'
    ],
    'role-2' => [
        'permission_data_src' => [
            'test_get' => [
                'boolAttrValue' => true
            ],
            'test_set' => [
                'boolAttrValue' => true
            ],
            'test-add-1' => [
                'boolAttrValue' => true
            ],
            'test-add-2' => [
                'boolAttrValue' => false
            ]
        ],
        'key-test-1' => 'Value test 1',
        'key-test-2' => 'Value test 2'
    ],
    'role-3' => []
);
$objRoleBuilder->setTabDataSrc($tabDataSrc);



// Test hydrate role entity collection
echo('Test hydrate role entity collection: <br />');

echo('Before hydrate: <pre>');
var_dump(getTabRoleEntityCollectionData($objRoleEntityCollection));
echo('</pre>');

$objRoleBuilder->hydrateRoleCollection($objRoleEntityCollection);

echo('After hydrate: <pre>');
var_dump(getTabRoleEntityCollectionData($objRoleEntityCollection));
echo('</pre>');

echo('<br /><br /><br />');



// Test get
echo('Test get role: <br />');

foreach($objRoleEntityCollection->getTabRoleName() as $strRoleName)
{
    echo('Test get role name "'.$strRoleName.'": <br />');

    $objRole = $objRoleEntityCollection->getObjRole($strRoleName);
    $objPermissionCollection = $objRole->getObjPermissionCollection();

    echo('Get role name: <pre>');var_dump($objRole->getStrRoleName());echo('</pre>');
    echo('Get role configuration: <pre>');var_dump($objRole->getTabRoleConfig());echo('</pre>');

    foreach($objRole->getObjPermissionCollection()->getTabPermissionKey() as $strKey)
    {
        echo('Check role, permission "'.$strKey.'" enable: <pre>');
        var_dump($objRole->checkPermissionEnable($strKey));
        echo('</pre>');

        echo('Check collection role, permission "'.$strKey.'" enable: <pre>');
        var_dump($objRoleEntityCollection->checkPermissionEnable($strKey));
        echo('</pre>');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


