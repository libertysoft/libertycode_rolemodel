<?php

namespace liberty_code\role_model\role\test;

use liberty_code\role_model\permission\subject\test\PermissionSubjectTest;
use liberty_code\role_model\role\subject\api\RoleSubjectInterface;



class RoleSubjectTest extends PermissionSubjectTest implements RoleSubjectInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



}