<?php
/**
 * GET arguments:
 * - clean_subject_role: = 1: Remove role references from role subjects.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/role/test/RoleRepositoryTest.php');
require_once($strRootAppPath . '/src/role/test/RoleSubjectTest.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
//use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\model\RoleEntity;
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\test\RoleSubjectTest;



// Init var
$getStrRoleRender = function(RoleEntity $objRoleEntity)
{
    $result = sprintf(
        'Role "%1$s"%2$s (Hash: "%3$s"):<br />',
        $objRoleEntity->getStrRoleName(),
        ($objRoleEntity->checkIsNew() ? ' [NEW]' : ''),
        spl_object_hash($objRoleEntity)
    );

    $objRolePermEntityCollection = $objRoleEntity->getObjPermissionCollection();
    foreach($objRolePermEntityCollection->getTabPermissionKey() as $strPermissionKey)
    {
        $objRolePermEntity = $objRolePermEntityCollection->getObjPermission($strPermissionKey);
        $result .= sprintf(
            '- Permission "%1$s"%2$s: %3$s<br />',
            $objRolePermEntity->getStrPermissionKey(),
            ($objRolePermEntity->checkIsNew() ? ' [NEW]' : ''),
            ($objRolePermEntity->checkPermissionEnable() ? '1' : '0')
        );
    }

    return $result;
};

$getStrRoleCollectionRender = function(RoleEntityCollection $objRoleEntityCollection) use ($getStrRoleRender)
{
    $result = '';

    foreach($objRoleEntityCollection->getTabRoleName() as $strRoleName)
    {
        $objRoleEntity = $objRoleEntityCollection->getObjRole($strRoleName);
        $result .= sprintf(
            '%1$s<br />',
            $getStrRoleRender($objRoleEntity)
        );
    }

    return $result;
};

$getStrSubjectRender = function(RoleSubjectTest $objSubject) use ($getStrRoleCollectionRender)
{
    /** @var RoleEntityCollection $objRoleEntityCollection */
    $objRoleEntityCollection = $objSubject->getObjRoleCollection();
    $result = sprintf(
        'Subject (type: "%1$s" - id: "%2$s"):<br />%3$s',
        $objSubject->getStrSubjectType(),
        $objSubject->getStrSubjectId(),
        $getStrRoleCollectionRender($objRoleEntityCollection)
    );

    return $result;
};

$getStrTabSubjectRender = function(array $tabSubject) use ($getStrSubjectRender)
{
    $result = '';

    foreach($tabSubject as $objSubject)
    {
        /** @var RoleSubjectTest $objSubject */
        $result .= sprintf(
            '%1$s<br /><br />',
            $getStrSubjectRender($objSubject)
        );
    }

    return $result;
};

$objSubject1 = new RoleSubjectTest(
    '1',
    null,
    new RoleEntityCollection()
);

$objSubject2 = new RoleSubjectTest(
    '2',
    null,
    new RoleEntityCollection()
);

$objSubject3 = new RoleSubjectTest(
    '3',
    null,
    new RoleEntityCollection()
);

$objSubject4 = new RoleSubjectTest(
    '4',
    null,
    new RoleEntityCollection()
);

/** @var RoleSubjectTest[] $tabSubject */
$tabSubject = array(
    $objSubject1,
    $objSubject2,
    $objSubject3,
    $objSubject4
);

$objRoleEntityCollection = new RoleEntityCollection();



// Init structure
echo('Test set structure: <pre>');
var_dump($objRolePermEntityRepo->setStructure());
var_dump($objRoleEntityRepo->setStructure());
echo('</pre>');

echo('<br /><br /><br />');



// Load roles
echo('Test load roles: <pre>');
$tabRoleId = array();
$objSelectCommand = $objRoleEntityCollectionRepo
    ->getObjRepository()
    ->getObjPersistor()
    ->getObjCommandFactory()
    ->getObjSelectCommand(
        array(
            'select' => [
                ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID
            ],
            'from' => [
                $objRoleEntityRepo->getStrSqlTableName()
            ]
        )
    );
$objSelectCommand
    ->setClauseSelect(array(ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID))
    ->setClauseFrom(array($objRoleEntityRepo->getStrSqlTableName()));
$objDbResult = $objSelectCommand->executeResult();
if($objDbResult !== false)
{
    while(($data = $objDbResult->getFetchData()) !== false)
    {
        $tabRoleId[] = intval($data[ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID]);
    }
    $objDbResult->close();
}
var_dump($objRoleEntityCollectionRepo->loadFromId($objRoleEntityCollection, $tabRoleId));
echo('</pre>');

// Build role, if required
$tabRoleName = array(
    'role_1',
    'role_2',
    'role_3'
);
foreach($tabRoleName as $strRoleName)
{
    if(!$objRoleEntityCollection->checkRoleExists($strRoleName))
    {
        $objRoleEntity = $objRoleEntityFactory->getObjRole(array('strAttrName' => $strRoleName));
        $objRoleEntityCollection->setRole($objRoleEntity);
    }
}

// Build permissions on each role, if required
foreach($objRoleEntityCollection->getTabRoleName() as $strRoleName)
{
    $objRoleEntity = $objRoleEntityCollection->getObjRole($strRoleName);
    $objRolePermSpecBuilder->hydratePermissionCollection($objRoleEntity->getObjPermissionCollection(), false);
}

echo($getStrRoleCollectionRender($objRoleEntityCollection));

echo('<br /><br /><br />');



// Save roles
foreach($objRoleEntityCollection->getTabRoleName() as $strRoleName)
{
    $objRoleEntity = $objRoleEntityCollection->getObjRole($strRoleName);
    $objRolePermEntityCollection = $objRoleEntity->getObjPermissionCollection();
    foreach($objRolePermEntityCollection->getTabPermissionKey() as $strPermissionKey)
    {
        $objRolePermEntity = $objRolePermEntityCollection->getObjPermission($strPermissionKey);
        $boolEnable = ((rand(0, 10) % 2) == 0);
        $objRolePermEntity->setPermissionEnable($boolEnable);
    }
}

foreach($objRoleEntityCollection->getTabKey() as $strKey)
{
    $objRoleEntity = $objRoleEntityCollection->getItem($strKey);
    //$objRoleEntity->setAttributeValue(ConstRole::ATTRIBUTE_KEY_ID, 77);
    //$objRoleEntity->setAttributeValue(ConstRole::ATTRIBUTE_KEY_NAME, 'role');
    $objRolePermEntityCollection = $objRoleEntity->getObjPermissionCollection();
    foreach($objRolePermEntityCollection->getTabKey() as $strRolePermEntityKey)
    {
        $objRolePermEntity = $objRolePermEntityCollection->getItem($strRolePermEntityKey);
        //$objRolePermEntity->setAttributeValue(ConstPermission::ATTRIBUTE_KEY_ID, 77);
    }
}

echo('Test save roles: ');
$tabError = array();
if($objRoleEntityCollection->checkValid(null, null, $tabError))
{
    echo('<pre>');var_dump($objRoleEntityCollectionRepo->save($objRoleEntityCollection));echo('</pre>');
}
else
{
    echo('Validation failed:<pre>');var_dump($tabError);echo('</pre>');
}

echo($getStrRoleCollectionRender($objRoleEntityCollection));

echo('<br /><br /><br />');



// Load subjects roles
echo('Test load subjects roles: <pre>');
var_dump($objRoleEntityCollectionRepo->loadSubjectRole($tabSubject));
echo('</pre>');

echo($getStrTabSubjectRender($tabSubject));

echo('<br /><br /><br />');



// Save subjects roles
$objRoleEntity1 = $objRoleEntityCollection->getObjRole('role_1');
$objRoleEntity2 = $objRoleEntityCollection->getObjRole('role_2');
$objRoleEntity3 = $objRoleEntityCollection->getObjRole('role_3');

if(!$objSubject1->getObjRoleCollection()->checkRoleExists('role_1'))
{
    $objSubject1->getObjRoleCollection()->setRole($objRoleEntity1);
}
if(!$objSubject1->getObjRoleCollection()->checkRoleExists('role_2'))
{
    $objSubject1->getObjRoleCollection()->setRole($objRoleEntity2);
}
if(!$objSubject1->getObjRoleCollection()->checkRoleExists('role_3'))
{
    $objSubject1->getObjRoleCollection()->setRole($objRoleEntity3);
}

if(!$objSubject2->getObjRoleCollection()->checkRoleExists('role_1'))
{
    $objSubject2->getObjRoleCollection()->setRole($objRoleEntity1);
}
if(!$objSubject2->getObjRoleCollection()->checkRoleExists('role_2'))
{
    $objSubject2->getObjRoleCollection()->setRole($objRoleEntity2);
}

if(!$objSubject3->getObjRoleCollection()->checkRoleExists('role_3'))
{
    $objSubject3->getObjRoleCollection()->setRole($objRoleEntity3);
}

echo('Test save subjects roles: <pre>');
var_dump($objRoleEntityCollectionRepo->saveSubjectRoleRef($tabSubject));
echo('</pre>');

echo($getStrTabSubjectRender($tabSubject));

echo('<br /><br /><br />');



// Remove subjects roles, if required
if(trim(ToolBoxTable::getItem($_GET, 'clean_subject_role', '0')) == '1')
{
    echo('Test remove subjects roles: <pre>');
    var_dump($objRoleEntityCollectionRepo->removeSubjectRoleRef($tabSubject));
    //var_dump($objRoleEntityCollectionRepo->removeSubjectRoleRefFromRole($objRoleEntityCollection));
    echo('</pre>');

    echo($getStrTabSubjectRender($tabSubject));

    echo('<br /><br /><br />');
}



// Remove test database, if required
require_once($strRootAppPath . '/src/permission/subject/test/db/HelpRemoveDbTest.php');


