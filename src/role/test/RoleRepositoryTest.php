<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/db/HelpCreateDbTest.php');
require_once($strRootAppPath . '/src/role/permission/test/RolePermRepositoryTest.php');
require_once($strRootAppPath . '/src/role/test/RoleTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;

use liberty_code\role_model\role\permission\model\RolePermEntityCollection;
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityRepository;
use liberty_code\role_model\role\sql\model\repository\SqlRoleEntityCollectionRepository;



// Init repository
$objRoleEntityRepo = new SqlRoleEntityRepository(
    $objTablePersistor,
    $objRolePermEntityCollectionRepo
);

$objPref = new Preference(array(
    'source' => SqlRoleEntityRepository::class,
    'set' =>  ['type' => 'instance', 'value' => $objRoleEntityRepo],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

// Init collection repository
$objRoleEntityCollectionRepo = new SqlRoleEntityCollectionRepository(
    $objRoleEntityFactory,
    $objRoleEntityRepo,
    new RolePermEntityCollection(),
    $objRolePermEntityCollectionRepo,
    new RoleEntityCollection
);


