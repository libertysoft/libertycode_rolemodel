<?php
/**
 * This class allows to define role entity factory class.
 * Role entity factory allows to design a role factory class,
 * to provide new role entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;
use liberty_code\role\role\factory\api\RoleFactoryInterface;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\role\api\RoleInterface;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\model\RoleEntity;



/**
 * @method RoleEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class RoleEntityFactory extends FixEntityFactory implements RoleFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RoleEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return RoleEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new RoleEntity(
            array(),
            $objValidator,
            $objDateTimeFactory
        );

        // Return result
        return $result;
    }



    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = $tabConfig;

        // Add configured key as role name, if required
        if(
            (!is_null($strConfigKey)) &&
            (!array_key_exists(ConstRole::ATTRIBUTE_KEY_NAME, $result))
        )
        {
            $result[ConstRole::ATTRIBUTE_KEY_NAME] = $strConfigKey;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRoleClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Return result
        return $this->getStrEntityClassPath();
    }



    /**
     * @inheritdoc
     * @return RoleEntity
     */
    public function getObjRole(
        array $tabConfig,
        $strConfigKey = null,
        RoleInterface $objRole = null
    )
    {
        // Init var
        $strClassPath = $this->getStrEntityClassPath();
        $result = (
            is_null($objRole) ?
                $this->getObjEntity() :
                (
                    (
                        is_subclass_of($objRole, $strClassPath) ||
                        get_class($objRole) == $strClassPath
                    ) ?
                        $objRole :
                        null
                )
        );

        // Hydrate role entity, if required
        if(!is_null($result))
        {
            $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
            $result->setRoleConfig($tabConfigFormat);
        }

        // Return result
        return $result;
    }



}