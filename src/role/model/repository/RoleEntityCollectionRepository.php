<?php
/**
 * This class allows to define role entity collection repository class.
 * Role entity collection repository allows to prepare data from role entity collection,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\role_model\role\permission\model\RolePermEntityCollection;
use liberty_code\role_model\role\permission\model\repository\RolePermEntityCollectionRepository;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\model\RoleEntityFactory;
use liberty_code\role_model\role\model\repository\RoleEntityRepository;
use liberty_code\role_model\role\subject\api\RoleSubjectInterface;



/**
 * @method null|RoleEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|RoleEntityRepository getObjRepository() @inheritdoc
 */
abstract class RoleEntityCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Role permission entity collection instance.
     * @var RolePermEntityCollection
     */
    protected $objRolePermEntityCollection;



    /**
     * DI: Role permission entity collection repository instance.
     * @var RolePermEntityCollectionRepository
     */
    protected $objRolePermEntityCollectionRepo;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RoleEntityFactory $objEntityFactory
     * @param RoleEntityRepository $objRepository
     * @param RolePermEntityCollection $objRolePermEntityCollection
     * @param RolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
     */
    public function __construct(
        RoleEntityFactory $objEntityFactory,
        RoleEntityRepository $objRepository,
        RolePermEntityCollection $objRolePermEntityCollection,
        RolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
    )
    {
        // Init properties
        $this->objRolePermEntityCollection = $objRolePermEntityCollection;
        $this->objRolePermEntityCollectionRepo = $objRolePermEntityCollectionRepo;

        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return RoleEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return RoleEntityRepository::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO => [
                ConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION => [
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY => $this->objRolePermEntityCollectionRepo,
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION => $this->objRolePermEntityCollection
                ]
            ],
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION => [
                ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE => false
            ]
        );
    }



    /**
     * Get index array of role criteria ids,
     * from specified index array of ids.
     *
     * @param integer[] $tabId
     * @return null|array
     */
    abstract protected function getTabCriteriaIdFromId($tabId);





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Update datetime create, update, on entities
        ToolBoxDateTime::hydrateEntityCollectionAttrDtCreateUpdate(
            $objCollection,
            ConstRole::ATTRIBUTE_KEY_DT_CREATE,
            ConstRole::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objCollection, $tabConfig, $tabInfo);
    }



    /**
     * Load role entity collection object,
     * from specified index array of ids.
     *
     * @param RoleEntityCollection $objRoleEntityCollection
     * @param integer[] $tabId
     * @return boolean
     */
    public function loadFromId(RoleEntityCollection $objRoleEntityCollection, array $tabId)
    {
        // Init var
        $tabCriteriaId = $this->getTabCriteriaIdFromId($tabId);
        $result = (
            (!is_null($tabCriteriaId)) ?
                $this->load(
                    $objRoleEntityCollection,
                    $tabCriteriaId,
                    array(
                        ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE => true
                    )
                ) :
                false
        );

        // Return result
        return $result;
    }


    


    // Methods subject repository
    // ******************************************************************************

    /**
     * Load role entities, on specified role subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * @param RoleSubjectInterface|RoleSubjectInterface[] $roleSubject
     * @return boolean
     */
    abstract public function loadSubjectRole($roleSubject);



    /**
     * Remove previous role references and save role references,
     * from specified role subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * @param RoleSubjectInterface|RoleSubjectInterface[] $roleSubject
     * @return boolean
     */
    abstract public function saveSubjectRoleRef($roleSubject);



    /**
     * Remove role references, from specified role subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * Remove all role references, from specified subjects, if option 'all' is true.
     * Else remove only role references in collections, from specified subjects.
     *
     * @param RoleSubjectInterface|RoleSubjectInterface[] $roleSubject
     * @param $boolAll = true
     * @return boolean
     */
    abstract public function removeSubjectRoleRef($roleSubject, $boolAll = true);



    /**
     * Remove role references, from specified role entity collection.
     * Return true if all role entities success, false if an error occurs on at least one role entity.
     *
     * @param RoleEntityCollection $objRoleEntityCollection
     * @return boolean
     */
    abstract public function removeSubjectRoleRefFromRole(RoleEntityCollection $objRoleEntityCollection);



}