<?php
/**
 * This class allows to define role entity repository class.
 * Role entity repository allows to prepare data from role entity,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\role_model\role\permission\library\ConstRolePerm;
use liberty_code\role_model\role\permission\model\repository\RolePermEntityCollectionRepository;
use liberty_code\role_model\role\library\ConstRole;
use liberty_code\role_model\role\model\RoleEntity;



abstract class RoleEntityRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Role permission entity collection repository instance.
     * @var RolePermEntityCollectionRepository
     */
    protected $objRolePermEntityCollectionRepo;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
     */
    public function __construct(
        PersistorInterface $objPersistor,
        RolePermEntityCollectionRepository $objRolePermEntityCollectionRepo
    )
    {
        // Init properties
        $this->objRolePermEntityCollectionRepo = $objRolePermEntityCollectionRepo;

        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array for persistence.
     * Overwrite it to implement specific configuration array.
     *
     * @return array
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array();
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $result = array_merge(
            array(
                ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RoleEntity::class,
                ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => ConstRole::ATTRIBUTE_KEY_ID,
                ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO => [
                    ConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION => [
                        ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY => $this->objRolePermEntityCollectionRepo,
                        ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING => [
                            ConstRolePerm::ATTRIBUTE_KEY_ROLE_ID => ConstRole::ATTRIBUTE_KEY_ID
                        ]
                    ]
                ],
                ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID
            ),
            $this->getTabFixPersistorConfig()
        );

        // Return result
        return $result;
    }



    /**
     * Get role criteria id,
     * from specified id.
     *
     * @param integer $intId
     * @return null|array
     */
    abstract protected function getCriteriaIdFromId($intId);





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidEntity($objEntity);

        // Update datetime create, update
        ToolBoxDateTime::hydrateEntityAttrDtCreateUpdate(
            $objEntity,
            ConstRole::ATTRIBUTE_KEY_DT_CREATE,
            ConstRole::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objEntity, $tabConfig, $tabInfo);
    }



    /**
     * Load role entity object,
     * from specified id.
     *
     * @param RoleEntity $objRoleEntity
     * @param integer $intId
     * @return boolean
     */
    public function loadFromId(RoleEntity $objRoleEntity, $intId)
    {
        // Init var
        $criteriaId = $this->getCriteriaIdFromId($intId);
        $result = (
            (!is_null($criteriaId)) ?
                $this->load(
                    $objRoleEntity,
                    $criteriaId,
                    array(
                        ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE => true
                    )
                ) :
                false
        );

        // Return result
        return $result;
    }



}


