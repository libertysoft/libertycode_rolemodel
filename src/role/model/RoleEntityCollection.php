<?php
/**
 * This class allows to define role entity collection class.
 * key: role name => role entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;
use liberty_code\role\role\api\RoleCollectionInterface;

use liberty_code\model\entity\exception\CollectionValueInvalidFormatException;
use liberty_code\role\role\api\RoleInterface;
use liberty_code\role_model\role\exception\NameNotFoundException;
use liberty_code\role_model\role\model\RoleEntity;



/**
 * @method null|RoleEntity getItem(string $strKey) @inheritdoc
 * @method string setItem(RoleEntity $objEntity) @inheritdoc
 */
class RoleEntityCollection extends FixEntityCollection implements RoleCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkRoleExists($strName)
    {
        // Return result
        return (!is_null($this->getObjRole($strName)));
    }



    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Init var
        $result = false;
        $tabRoleEntity = array_values($this->getTabItem());

        // Run each role entity
        for($intCpt = 0; ($intCpt < count($tabRoleEntity)) && (!$result); $intCpt++)
        {
            /** @var RoleEntity $objRoleEntity */
            $objRoleEntity = $tabRoleEntity[$intCpt];

            // Check permission enabled
            $result = $objRoleEntity->checkPermissionEnable($strKey);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return RoleEntity::class;
    }



    /**
     * Get role entity collection key,
     * from specified role name.
     *
     * @param string $strName
     * @param boolean $boolThrow = true
     * @return null|string
     * @throws NameNotFoundException
     */
    protected function getStrKeyFromName($strName, $boolThrow = true)
    {
        // Init var
        $result = null;
        $tabKey = $this->getTabKey();

        // Run each entities
        for($intCpt = 0; ($intCpt < count($tabKey)) && is_null($result); $intCpt++)
        {
            $strKey = $tabKey[$intCpt];
            $objRoleEntity = $this->getItem($strKey);

            // Register key, if found
            $result = (
            (
                is_string($strName) &&
                ($objRoleEntity->getStrRoleName() == $strName)
            ) ?
                $strKey :
                null
            );
        }

        // Throw exception, if required
        if(is_null($result) && $boolThrow)
        {
            throw new NameNotFoundException($strName);
        }

        // Return result
        return $result;
    }



    /**
     * Get role name,
     * from specified role entity collection key.
     *
     * @param string $strKey
     * @return null|string
     */
    protected function getStrNameFromKey($strKey)
    {
        // Init var
        $result = null;
        $objRoleEntity = $this->getItem($strKey);
        $result = (
            (!is_null($objRoleEntity)) ?
                $objRoleEntity->getStrRoleName() :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabRoleName()
    {
        // Init var
        $result = $this->getTabKey();
        $result = array_map(
            function($strKey)
            {
                return $this->getStrNameFromKey($strKey);
            },
            $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return null|RoleEntity
     */
    public function getObjRole($strName)
    {
        // Init var
        $strKey = $this->getStrKeyFromName($strName, false);
        $result = (
            (!is_null($strKey)) ?
                $this->getItem($strKey) :
                null
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RoleEntity $objRole
     * @throws CollectionValueInvalidFormatException
     */
    public function setRole(RoleInterface $objRole)
    {
        // Check argument
        if(!($objRole instanceof RoleEntity))
        {
            throw new CollectionValueInvalidFormatException($objRole);
        }

        // Init var
        /** @var RoleEntity $objRole */
        $strKey = $this->setItem($objRole);
        $result = $this->getStrNameFromKey($strKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param array|static $tabRole
     */
    public function setTabRole($tabRole)
    {
        // Init var
        $tabKey = $this->setTabItem($tabRole);
        $result = array_map(
            function($strKey)
            {
                return $this->getStrNameFromKey($strKey);
            },
            $tabKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return RoleEntity
     */
    public function removeRole($strName)
    {
        // Init var
        $strKey = $this->getStrKeyFromName($strName);

        // return result
        return $this->removeItem($strKey);
    }



    /**
     * @inheritdoc
     */
    public function removeRoleAll()
    {
        $this->removeItemAll();
    }



}