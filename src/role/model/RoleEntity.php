<?php
/**
 * This class allows to define role entity class.
 * Role entity allows to design a role class,
 * using entity.
 *
 * Role entity uses the following specified configuration:
 * [
 *     @see RoleEntity::hydrate():
 *         List of attributes hydration: @see RoleEntity::getTabConfig()
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\role\role\api\RoleInterface;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role_model\role\permission\model\RolePermEntityCollection;
use liberty_code\role_model\role\library\ConstRole;



/**
 * @property integer $intAttrId
 * @property string|DateTime $attrDtCreate
 * @property string|DateTime $attrDtUpdate
 * @property string $strAttrName
 * @property RolePermEntityCollection $objAttrRolePermEntityCollection
 */
class RoleEntity extends SaveConfigEntity implements RoleInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Call parent constructor
        parent::__construct($tabValue, $objValidator);

        // Init datetime factory repository
        $this->setDateTimeFactory($objDateTimeFactory);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstRole::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $objDtNow = new DateTime();
        $objDtNow = (
            (!is_null($objDtFactory)) ?
                $objDtFactory->getObjRefDt($objDtNow) :
                $objDtNow
        );

        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRole::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRole::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRole::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRole::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $objDtNow
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRole::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRole::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $objDtNow
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRole::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRole::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_NM,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ],

            // Attribute permission entity collection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRole::ATTRIBUTE_ALIAS_ROLE_PERM_ENTITY_COLLECTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => false,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new RolePermEntityCollection()
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            ConstRole::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                    ]
                ]
            ],
            ConstRole::ATTRIBUTE_KEY_DT_CREATE => [
                'type_date'
            ],
            ConstRole::ATTRIBUTE_KEY_DT_UPDATE => [
                'type_date'
            ],
            ConstRole::ATTRIBUTE_KEY_NAME => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ],
            ConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION => [
                [
                    'type_object',
                    [
                        'class_path' => [RolePermEntityCollection::class],
                        'extend_enable_require' => false,
                        'error_message_pattern' => '%1$s must be a valid role permission collection.'
                    ]
                ],
                ['validation_entity_collection']
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstRole::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRole::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstRole::ATTRIBUTE_KEY_ID:
                $result = (
                    (is_string($value) && ctype_digit($value)) ?
                        intval($value) :
                        $value
                );
                break;

            case ConstRole::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRole::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSet($value, true) :
                        (
                            ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                $objDtFactory->getObjRefDt($value) :
                                $value
                        )
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstRole::ATTRIBUTE_KEY_ID:
                $result = strval($value);
                break;

            case ConstRole::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRole::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstRole::ATTRIBUTE_KEY_ID:
                $result = intval($value);
                break;

            case ConstRole::ATTRIBUTE_KEY_DT_CREATE:
            case ConstRole::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        (
                            ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                $objDtFactory->getObjRefDt($value) :
                                $value
                        )
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Return result
        return $this->getObjPermissionCollection()->checkPermissionEnable($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get datetime factory object.
     *
     * @return null|DateTimeFactoryInterface
     */
    public function getObjDateTimeFactory()
    {
        // Return result
        return $this->objDateTimeFactory;
    }



    /**
     * @inheritdoc
     * @return RolePermEntityCollection
     */
    public function getObjPermissionCollection()
    {
        // Return result
        return $this->getAttributeValue(ConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION);
    }



    /**
     * @inheritdoc
     */
    public function getStrRoleName()
    {
        // Return result
        return $this->getAttributeValue(ConstRole::ATTRIBUTE_KEY_NAME);
    }



    /**
     * Get formatted role configuration array.
     *
     * @param array $tabConfig
     * @return array
     */
    protected function getTabRoleConfigFormat(array $tabConfig)
    {
        // Init var
        $result = array();

        // Map specific attributes
        $tabAttrKey = array(
            ConstRole::ATTRIBUTE_KEY_NAME,
        );
        foreach($tabAttrKey as $strAttrKey)
        {
            if(array_key_exists($strAttrKey, $tabConfig))
            {
                $result[$strAttrKey] = $tabConfig[$strAttrKey];
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabRoleConfig()
    {
        // Init var
        $result = $this->getTabData(false);
        $result = $this->getTabRoleConfigFormat($result);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set datetime factory object.
     *
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function setDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory = null)
    {
        $this->objDateTimeFactory = $objDateTimeFactory;
    }



    /**
     * @inheritdoc
     * @param RolePermEntityCollection $objPermissionCollection
     */
    public function setPermissionCollection(PermissionCollectionInterface $objPermissionCollection)
    {
        // Set value
        $this->setAttributeValue(ConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION, $objPermissionCollection);
    }



    /**
     * @inheritdoc
     */
    public function setRoleConfig(array $tabConfig)
    {
        // Init var
        $tabConfig = $this->getTabRoleConfigFormat($tabConfig);

        // Hydrate attributes
        $this->hydrate($tabConfig);
    }



}