<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/role/browser/test/RoleBrowserRepositoryTest.php');

$strGetClean = (isset($_GET['clean']) ? $_GET['clean'] : '0');
$_GET['clean'] = '0';
ob_start();
require_once($strRootAppPath . '/src/role/test/TestRepository.php');
ob_get_clean();
$objConnection->connect(); // Reconnect database (previously de-connected)

// Use
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\browser\model\RoleBrowserEntity;



// Test browser load
$objDtNow = new DateTime('now', new DateTimeZone('America/New_York'));
$objDtStart = (clone $objDtNow)->sub(new DateInterval('P1D'));
$objDtEnd = (clone $objDtNow)->add(new DateInterval('P1D'));
$tabTabData = array(
    [
        'intAttrItemCountPage' => 1,
        'intAttrPageIndex' => 0,
        'strAttrSortName' => 'desc'
    ], // Ok: show role_3
    [
        'intAttrItemCountPage' => 2,
        'intAttrPageIndex' => 0,
        'strAttrCritLikeName' => 'role',
        'strAttrSortName' => 'asc'
    ], // Ok: show role_1, role_2
    [
        'strAttrCritEqualName' => 'role_2',
    ], // Ok: show role_2
    [
        'intAttrItemCountPage' => 1,
        'intAttrPageIndex' => 1,
        'tabAttrCritInName' => ['role_1', 'role_2', 'test'],
        'strAttrSortName' => 'desc'
    ], // Ok: show role_1
    [
        'attrCritStartDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritStartDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u')
    ], // Ok: show nothing
    [
        //'attrCritStartDtCreate' => $objDtStart->format('Y-m-d H:i:s.u'),
        //'attrCritEndDtCreate' => $objDtEnd->format('Y-m-d H:i:s.u'),
        'attrCritStartDtUpdate' => $objDtStart->format('Y-m-d H:i:s.u'),
        'attrCritEndDtUpdate' => $objDtEnd->format('Y-m-d H:i:s.u')
    ] // Ok: show role_1, role_2, role_3
);
echo('Test browser load: <br />');

foreach($tabTabData as $tabData)
{
    echo('Browser data: <pre>');
    print_r($tabData);
    echo('</pre>');

    try
    {
        // Set browser entity data
        $objRoleBrowserEntity = new RoleBrowserEntity(
            array(),
            $objValidator,
            $objDateTimeFactory
        );
        $objRoleBrowserEntity->hydrate($tabData);
        $objRoleBrowserEntity->setAttributeValid();

        // Load role entities
        $objRoleEntityCollection = new RoleEntityCollection();
        $tabBrowserInfo = array();
        $objRoleBrowserRepo->loadFromEntity(
            $objRoleEntityCollection,
            $objRoleBrowserEntity,
            array(),
            $tabBrowserInfo
        );

        echo($getStrRoleCollectionRender($objRoleEntityCollection));
        echo('<br />');

        echo('Browser info: <pre>');
        print_r($tabBrowserInfo);
        echo('</pre>');
    }
    catch(Exception $e)
    {
        echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Remove test database, if required
$_GET['clean'] = $strGetClean;
require($strRootAppPath . '/src/permission/subject/test/db/HelpRemoveDbTest.php');


