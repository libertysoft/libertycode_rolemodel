<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/role/permission/test/RolePermTest.php');

// Use
use liberty_code\role_model\role\browser\model\RoleBrowserEntity;



// Init var
$objRoleBrowserEntity = new RoleBrowserEntity(
    array(),
    $objValidator,
    $objDateTimeFactory
);



// Test set attribute
$objDtNow = new DateTime('now', new DateTimeZone('America/New_York'));
$tabTabData = array(
    [
        'intAttrItemCountPage' => 0,
        'intAttrPageIndex' => -3,
        'intAttrCritEqualId' => 'test',
        'tabAttrCritInId' => [1, 'test'],
        'strAttrCritLikeName' => 7,
        'strAttrCritEqualName' => '',
        'tabAttrCritInName' => ['', 'test-2'],
        'attrCritStartDtCreate' => 'test',
        'attrCritEndDtCreate' => true,
        'attrCritStartDtUpdate' => false,
        'attrCritEndDtUpdate' => 7,
        'strAttrSortId' => 'test',
        'strAttrSortDtCreate' => 'test',
        'strAttrSortDtUpdate' => 'test',
        'strAttrSortName' => 7
    ],
    [
        'intAttrItemCountPage' => 50,
        'intAttrPageIndex' => 0,
        'intAttrCritEqualId' => 1,
        'tabAttrCritInId' => [1, 3],
        'strAttrCritLikeName' => 'test',
        'strAttrCritEqualName' => 'test',
        'tabAttrCritInName' => ['test', 'test-2'],
        'attrCritStartDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtCreate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritStartDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'attrCritEndDtUpdate' => $objDtNow->format('Y-m-d H:i:s.u'),
        'strAttrSortId' => 'asc',
        'strAttrSortDtCreate' => 'desc',
        'strAttrSortDtUpdate' => 'asc',
        'strAttrSortName' => 'desc'
    ],
);
echo('Test set attribute: <br />');

foreach($tabTabData as $tabData)
{
    echo('Set data: <pre>');
    print_r($tabData);
    echo('</pre>');

    try
    {
        $objRoleBrowserEntity->hydrate($tabData);

        foreach($objRoleBrowserEntity->getTabAttributeKey() as $strKey)
        {
            try
            {
                echo('Check "'.$strKey.'": <pre>');
                var_dump($objRoleBrowserEntity->setAttributeValid($strKey));
                echo('</pre>');
            }
            catch(Exception $e)
            {
                echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
            }
        }
    }
    catch(Exception $e)
    {
        echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
    }
    finally
    {
        echo('Validation done!<br /><br />');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get attribute
echo('Test get attribute: <pre>');
print_r($objRoleBrowserEntity->getTabData());
echo('</pre>');

echo('<br /><br /><br />');


