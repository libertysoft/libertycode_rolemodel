<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/role/test/RoleRepositoryTest.php');

// Use
use liberty_code\role_model\role\browser\sql\model\repository\SqlRoleBrowserRepository;



// Init repository
$objRoleBrowserRepo = new SqlRoleBrowserRepository(
    $objTableBrowser,
    $objRoleEntityCollectionRepo
);


