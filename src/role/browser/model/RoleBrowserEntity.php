<?php
/**
 * This class allows to define role browser entity class.
 * Role browser entity allows to define attributes,
 * to search role entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\browser\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\role_model\role\browser\library\ConstRoleBrowser;



/**
 * @property integer $intAttrItemCountPage
 * @property integer $intAttrPageIndex
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 */
class RoleBrowserEntity extends ValidatorConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Call parent constructor
        parent::__construct($tabValue, $objValidator);

        // Init datetime factory repository
        $this->setDateTimeFactory($objDateTimeFactory);
    }





    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute page count
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_ITEM_COUNT_PAGE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 10
            ],

            // Attribute page index
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_PAGE_INDEX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_PAGE_INDEX,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 0
            ],

            // Attribute criteria equal id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria in id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria like name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria equal name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria in name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria start datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria end datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria start datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute criteria end datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_SORT_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute sort name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleBrowser::ATTRIBUTE_KEY_SORT_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Get rule configuration
        $tabRuleConfigNullValidInteger = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );

        $tabRuleConfigNullValidTabInteger = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );

        $tabRuleConfigNullValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty'],
                                    'error_message_pattern' => '%1$s is empty.'
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty.'
                ]
            ]
        );

        $tabRuleConfigNullValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );

        $tabRuleConfigNullValidDateTime = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );

        $tabSort = array(
            ConstOrderClause::OPERATOR_CONFIG_ASC,
            ConstOrderClause::OPERATOR_CONFIG_DESC
        );
        $strTabSort = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabSort
        ));
        $tabRuleConfigNullValidSortString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-sort' => [
                            [
                                'compare_in',
                                [
                                    'compare_value' => $tabSort
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid sort string (' . $strTabSort . ').'
                ]
            ]
        );

        $result = array(
            ConstRoleBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE => [
                [
                    'type_numeric',
                    ['integer_only_require' => true]
                ],
                [
                    'compare_between',
                    [
                        'greater_compare_value' => 0,
                        'greater_equal_enable_require' => false,
                        'less_compare_value' => 100
                    ]
                ]
            ],
            ConstRoleBrowser::ATTRIBUTE_KEY_PAGE_INDEX => [
                [
                    'type_numeric',
                    ['integer_only_require' => true]
                ],
                [
                    'compare_greater',
                    ['compare_value' => 0]
                ]
            ],
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigNullValidInteger,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigNullValidTabInteger,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigNullValidString,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigNullValidString,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigNullValidTabString,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigNullValidDateTime,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigNullValidDateTime,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigNullValidDateTime,
            ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigNullValidDateTime,
            ConstRoleBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigNullValidSortString,
            ConstRoleBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigNullValidSortString,
            ConstRoleBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigNullValidSortString,
            ConstRoleBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigNullValidSortString
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstRoleBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_PAGE_INDEX:
                $result = (
                    (is_string($value) && ctype_digit($value)) ?
                        intval($value) :
                        $value
                );
                break;

            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstRoleBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstRoleBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_SORT_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get datetime factory object.
     *
     * @return null|DateTimeFactoryInterface
     */
    public function getObjDateTimeFactory()
    {
        // Return result
        return $this->objDateTimeFactory;
    }



    /**
     * Get array of browser data (Attribute key => attribute value).
     *
     * @return array
     */
    public function getTabBrowserData()
    {
        // Init var
        $result = array();
        $objDtFactory = $this->getObjDateTimeFactory();

        // Run all attribute keys
        foreach($this->getTabAttributeKey() as $strKey)
        {
            // Get attribute value
            switch($strKey)
            {
                case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
                case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
                case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
                case ConstRoleBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                    $value = $this->getAttributeValue($strKey, false);
                    $value = (
                        ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                            $objDtFactory->getStrSaveDt($value) :
                            $value
                    );
                    break;

                default:
                    $value = $this->getAttributeValue($strKey);
                    break;
            }

            // Register attribute in result
            $result[$strKey] = $value;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set datetime factory object.
     *
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function setDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory = null)
    {
        $this->objDateTimeFactory = $objDateTimeFactory;
    }



}