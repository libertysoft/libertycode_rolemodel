<?php
/**
 * This class allows to define role browser repository class.
 * Role browser repository allows to load role entities,
 * from specified role browser entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\browser\model\repository;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\role_model\role\model\repository\RoleEntityCollectionRepository;
use liberty_code\role_model\role\model\RoleEntityCollection;
use liberty_code\role_model\role\browser\library\ConstRoleBrowser;
use liberty_code\role_model\role\browser\model\RoleBrowserEntity;



abstract class RoleBrowserRepository extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Role entity collection repository instance.
     * @var RoleEntityCollectionRepository
     */
    protected $objRoleEntityCollectionRepo;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     * @param RoleEntityCollectionRepository $objRoleEntityCollectionRepo
     */
	public function __construct(RoleEntityCollectionRepository $objRoleEntityCollectionRepo)
	{
        // Init properties
        $this->objRoleEntityCollectionRepo = $objRoleEntityCollectionRepo;

		// Call parent constructor
		parent::__construct();
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of role ids,
     * from specified associative array of browser data.
     *
     * Browser data format: @see load()
     *
     * @param array $tabBrowserData
     * @return null|array
     */
    abstract protected function getTabId(array $tabBrowserData);





    // Methods repository
    // ******************************************************************************

    /**
     * Load specified role entity collection object,
     * from specified associative array of browser data.
     * Return true if all role entities success, false if an error occurs on at least one role entity.
     *
     * Browser data format:
     * [
     *     @see RoleBrowserEntity attributes list,
     *
     *     tabCritNotInId => [integer role id 1, ..., integer role id N]
     * ]
     *
     * Browser information returned format:
     * [
     *     item_count_page => integer (count of item, per page),
     *
     *     item_count_active_page => integer (count of item, on active page),
     *
     *     page_index => integer (index of active page)
     * ]
     *
     * @param RoleEntityCollection $objRoleEntityCollection
     * @param array $tabBrowserData
     * @param array &$tabBrowserInfo = array()
     * @return boolean
     */
    public function load(
        RoleEntityCollection $objRoleEntityCollection,
        array $tabBrowserData,
        array &$tabBrowserInfo = array()
    )
    {
        // Init var
        $tabId = $this->getTabId($tabBrowserData);
        $result = (
            is_array($tabId) ?
                $this->objRoleEntityCollectionRepo->loadFromId($objRoleEntityCollection, $tabId) :
                false
        );
        $tabBrowserInfo = (
            $result ?
                array(
                    ConstRoleBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT_PAGE => (
                        isset($tabBrowserData[ConstRoleBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE]) ?
                            $tabBrowserData[ConstRoleBrowser::ATTRIBUTE_KEY_ITEM_COUNT_PAGE] :
                            0
                    ),
                    ConstRoleBrowser::TAB_BROWSE_INFO_KEY_ITEM_COUNT_ACTIVE_PAGE => $objRoleEntityCollection->count(),
                    ConstRoleBrowser::TAB_BROWSE_INFO_KEY_PAGE_INDEX => (
                        isset($tabBrowserData[ConstRoleBrowser::ATTRIBUTE_KEY_PAGE_INDEX]) ?
                            $tabBrowserData[ConstRoleBrowser::ATTRIBUTE_KEY_PAGE_INDEX] :
                            0
                    )
                ) :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * Load specified role entity collection object,
     * from specified role browser entity.
     * Return true if all role entities success, false if an error occurs on at least one role entity.
     *
     * Additive browser data format: @see load() browser data format.
     *
     * Browser information returned format: @see load() browser information format.
     *
     * @param RoleEntityCollection $objRoleEntityCollection
     * @param RoleBrowserEntity $objRoleBrowserEntity
     * @param array $tabAddBrowserData = array()
     * @param array &$tabBrowserInfo = array()
     * @return boolean
     */
    public function loadFromEntity(
        RoleEntityCollection $objRoleEntityCollection,
        RoleBrowserEntity $objRoleBrowserEntity,
        array $tabAddBrowserData = array(),
        array &$tabBrowserInfo = array()
    )
    {
        // Init var
        $tabBrowserData = array_merge(
            $objRoleBrowserEntity->getTabBrowserData(),
            $tabAddBrowserData
        );
        $result = $this->load($objRoleEntityCollection, $tabBrowserData, $tabBrowserInfo);

        // Return result
        return $result;
    }



}