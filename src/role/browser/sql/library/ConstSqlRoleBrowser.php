<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\role\browser\sql\library;



class ConstSqlRoleBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
    
    // Browser information configuration
    const TAB_BROWSE_INFO_KEY_ITEM_COUNT = 'item_count';
    const TAB_BROWSE_INFO_KEY_PAGE_COUNT = 'page_count';
}