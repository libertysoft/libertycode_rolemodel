<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\library;



class ConstPermission
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_KEY = 'strAttrKey';
    const ATTRIBUTE_KEY_VALUE = 'boolAttrValue';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_KEY = 'key';
    const ATTRIBUTE_ALIAS_VALUE = 'value';

    const ATTRIBUTE_NAME_SAVE_PERM_ID = 'perm_id';
    const ATTRIBUTE_NAME_SAVE_PERM_DT_CREATE = 'perm_dt_create';
    const ATTRIBUTE_NAME_SAVE_PERM_DT_UPDATE = 'perm_dt_update';
    const ATTRIBUTE_NAME_SAVE_PERM_KEY = 'perm_key';
    const ATTRIBUTE_NAME_SAVE_PERM_VALUE = 'perm_value';



    // Exception message constants
    const EXCEPT_MSG_PERMISSION_KEY_NOT_FOUND = 'Following permission key "%1$s" not found in permission collection.';
}