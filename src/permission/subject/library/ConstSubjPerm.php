<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\library;



class ConstSubjPerm
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_SUBJ_TYPE = 'strAttrSubjType';
    const ATTRIBUTE_KEY_SUBJ_ID = 'strAttrSubjId';

    const ATTRIBUTE_ALIAS_SUBJ_TYPE = 'subject-type';
    const ATTRIBUTE_ALIAS_SUBJ_ID = 'subject-id';

    const ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE = 'perm_subj_type';
    const ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID = 'perm_subj_id';



    // Exception message constants
    const EXCEPT_MSG_TAB_SUBJECT_INVALID_FORMAT =
        'Following array subjects "%1$s" invalid! It must be an index array of permission subject objects.';
}