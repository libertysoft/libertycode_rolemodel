<?php
/**
 * Description :
 * This class allows to describe behavior of permission subject class.
 * Permission subject is permission subject item, can be used in subject permission entities.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\api;

use liberty_code\role\permission\subject\api\PermissionSubjectInterface as BasePermissionSubjectInterface;
use liberty_code\role_model\subject\api\SubjectInterface;



interface PermissionSubjectInterface extends BasePermissionSubjectInterface, SubjectInterface
{

}