<?php
/**
 * This class allows to define SQL subject permission entity repository class.
 * SQL subject permission entity repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\sql\model\repository;

use liberty_code\role_model\permission\subject\model\repository\SubjPermEntityRepository;

use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\subject\library\ConstSubjPerm;
use liberty_code\role_model\permission\subject\sql\library\ConstSqlSubjPerm;



/**
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlSubjPermEntityRepository extends SubjPermEntityRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TablePersistor $objPersistor = null
     */
    public function __construct(TablePersistor $objPersistor = null)
    {
        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return TablePersistor::class;
    }



    /**
     * Get SQL table name where subject permissions stored.
     * Overwrite it to implement specific table name.
     *
     * @return string
     */
    public function getStrSqlTableName()
    {
        // Return result
        return ConstSqlSubjPerm::SQL_TABLE_NAME_SUBJ_PERM;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array(
            ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME => $this->getStrSqlTableName(),
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID  => ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID,
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set subject permission structure.
     * Return true if correctly run, false else.
     *
     * @return boolean
     */
    public function setStructure()
    {
        // Init var
        $objConnection = $this
            ->getObjPersistor()
            ->getObjCommandFactory()
            ->getObjConnection();

        // Create subject permission table
        $strSql = sprintf(
            'CREATE TABLE %1$s (
                %2$s int(10) unsigned NOT NULL AUTO_INCREMENT,
                %3$s datetime NOT NULL,
                %4$s datetime NOT NULL,
                %5$s varchar(100) NOT NULL,
                %6$s tinyint(1) NOT NULL,
                %7$s varchar(100) NOT NULL,
                %8$s varchar(100) NOT NULL,
                PRIMARY KEY (%2$s)
            );',
            $objConnection->getStrEscapeName($this->getStrSqlTableName()),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_DT_CREATE),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_DT_UPDATE),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_KEY),
            $objConnection->getStrEscapeName(ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_VALUE),
            $objConnection->getStrEscapeName(ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE),
            $objConnection->getStrEscapeName(ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID)
        );
        $result = ($objConnection->execute($strSql) !== false);

        // Create indexes, if required
        if($result)
        {
            // Create subject type index
            $strSql = sprintf(
                'CREATE INDEX %1$s ON %2$s (%1$s);',
                $objConnection->getStrEscapeName(ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE),
                $objConnection->getStrEscapeName($this->getStrSqlTableName())
            );
            $result = ($objConnection->execute($strSql) !== false);

            // Create subject id index
            $strSql = sprintf(
                'CREATE INDEX %1$s ON %2$s (%1$s);',
                $objConnection->getStrEscapeName(ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID),
                $objConnection->getStrEscapeName($this->getStrSqlTableName())
            );
            $result = ($objConnection->execute($strSql) !== false) && $result;
        }

        // Return result
        return $result;
    }



}


