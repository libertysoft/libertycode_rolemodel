<?php
/**
 * This class allows to define SQL subject permission entity collection repository class.
 * SQL subject permission entity collection repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\sql\model\repository;

use liberty_code\role_model\permission\subject\model\repository\SubjPermEntityCollectionRepository;

use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\subject\library\ConstSubjPerm;
use liberty_code\role_model\permission\subject\api\PermissionSubjectInterface;
use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection;
use liberty_code\role_model\permission\subject\model\SubjPermEntityFactory;
use liberty_code\role_model\permission\subject\sql\model\repository\SqlSubjPermEntityRepository;
use liberty_code\role_model\permission\subject\exception\TabSubjectInvalidFormatException;



/**
 * @method null|SqlSubjPermEntityRepository getObjRepository() @inheritdoc
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlSubjPermEntityCollectionRepository extends SubjPermEntityCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Subject permission entity collection instance.
     * @var null|SubjPermEntityCollection
     */
    protected $objSubjPermEntityCollection;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlSubjPermEntityRepository $objRepository = null
     * @param SubjPermEntityCollection $objSubjPermEntityCollection = null
     */
    public function __construct(
        SubjPermEntityFactory $objEntityFactory = null,
        SqlSubjPermEntityRepository $objRepository = null,
        SubjPermEntityCollection $objSubjPermEntityCollection = null
    )
    {
        // Init properties
        $this->objSubjPermEntityCollection = $objSubjPermEntityCollection;

        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return SqlSubjPermEntityRepository::class;
    }





    // Methods subject getters
    // ******************************************************************************

    /**
     * Get permission subject key (can be used as identifier),
     * from specified subject type,
     * and subject id.
     *
     * @param string $strSubjectType
     * @param string $strSubjectId
     * @return string
     */
    protected function getStrSubjectKey($strSubjectType, $strSubjectId)
    {
        // Return result
        return sprintf(
            '%1$s_%2$s',
            $strSubjectType,
            $strSubjectId
        );
    }



    /**
     * Get array of permission ids, per permission subject,
     * from specified permission subjects.
     *
     * Array return format:
     * [
     *     // Index array of subject permission ids, for permission subject 1
     *     'string permission subject type' . '-' . 'string permission subject id' => [
     *         integer subject permission id 1,
     *         ...,
     *         integer subject permission id N
     *     ],
     *
     *     ...,
     *
     *     // Index array of subject permission ids, for permission subject N
     *     [...]
     * ]
     *
     * @param PermissionSubjectInterface[] $tabPermissionSubject
     * @return null|array
     */
    protected function getTabIdFromSubject(array $tabPermissionSubject)
    {
        // Init var
        $result = array();

        if(count($tabPermissionSubject) > 0)
        {
            // Init var
            $result = null;
            $objConnection = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjConnection();
            $objSelectCommand = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjSelectCommand();

            // Get SQL arguments
            $strTableNm = $this->getObjRepository()->getStrSqlTableName();

            $tabSqlType = array_map(
                function($objPermissionSubject) use ($objConnection) {
                    /** @var PermissionSubjectInterface $objPermissionSubject */
                    return $objConnection->getStrEscapeValue($objPermissionSubject->getStrSubjectType());
                },
                $tabPermissionSubject
            );
            $tabSqlType = array_unique($tabSqlType);
            $strTabSqlType = implode(', ', $tabSqlType);

            $tabSqlId = array_map(
                function($objPermissionSubject) use ($objConnection) {
                    /** @var PermissionSubjectInterface $objPermissionSubject */
                    return $objConnection->getStrEscapeValue($objPermissionSubject->getStrSubjectId());
                },
                $tabPermissionSubject
            );
            $strTabSqlId = implode(', ', $tabSqlId);

            // Build select command
            $objSelectCommand
                ->setClauseSelect(
                    array(
                        ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE,
                        ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID,
                        ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID
                    )
                )
                ->setClauseFrom(array($strTableNm))
                ->setClauseWhere(
                    array(
                        'content' => [
                            [
                                'operand' => ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE,
                                'operator' => 'in',
                                'value' => ['pattern' => $strTabSqlType]
                            ],
                            [
                                'operand' => ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID,
                                'operator' => 'in',
                                'value' => ['pattern' => $strTabSqlId]
                            ]
                        ]
                    )
                );

            // Execute query
            $objDbResult = $objSelectCommand->executeResult();

            // Run query result
            if($objDbResult !== false)
            {
                $result = array();
                while(($data = $objDbResult->getFetchData()) !== false)
                {
                    // Get data
                    $strSubjectKey = $this->getStrSubjectKey(
                        $data[ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE],
                        $data[ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID]
                    );
                    $intPermId = intval($data[ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID]);

                    // Init result, if required
                    if (!array_key_exists($strSubjectKey, $result)) $result[$strSubjectKey] = array();

                    // Set data in result
                    $result[$strSubjectKey][] = $intPermId;
                }

                // Close query
                $objDbResult->close();
            }
        }

        // Return result
        return $result;
    }





    // Methods subject repository
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws TabSubjectInvalidFormatException
     */
    public function loadSubjectPermission($permissionSubject)
    {
        // Init var
        $tabPermissionSubject = (
            is_array($permissionSubject)?
                array_values($permissionSubject) :
                array($permissionSubject)
        );

        // Set check argument
        TabSubjectInvalidFormatException::setCheck($tabPermissionSubject);

        // Init var
        $result = false;
        $tabIdPerSubj = $this->getTabIdFromSubject($tabPermissionSubject);
        $objSubjPermEntityCollection = $this->objSubjPermEntityCollection;

        // Init global subject permission entity collection, if required
        if(!is_null($objSubjPermEntityCollection))
        {
            $tabPermId = array();
            foreach($tabIdPerSubj as $strSubjectKey => $tabId)
            {
                foreach($tabId as $intId)
                {
                    if(!in_array($intId, $tabPermId))
                    {
                        $tabPermId[] = $intId;
                    }
                }
            }

            $objSubjPermEntityCollection->removeItemAll();
            $result = $this->load($objSubjPermEntityCollection, $tabPermId);
        }

        // Run each permission subject
        foreach($tabPermissionSubject as $objPermissionSubject)
        {
            /** @var PermissionSubjectInterface $objPermissionSubject */
            $objSubjectSubjPermEntityCollection = $objPermissionSubject->getObjPermissionCollection();

            // Check valid subject permission collection
            if($objSubjectSubjPermEntityCollection instanceof SubjPermEntityCollection)
            {
                // Get index array of permission ids
                $strSubjectKey = $this->getStrSubjectKey(
                    $objPermissionSubject->getStrSubjectType(),
                    $objPermissionSubject->getStrSubjectId()
                );

                // Check subject permissions found, for permission subject
                if(array_key_exists($strSubjectKey, $tabIdPerSubj))
                {
                    $tabId = $tabIdPerSubj[$strSubjectKey];

                    // Get subject permissions from global collection, if required
                    if(!is_null($objSubjPermEntityCollection))
                    {
                        if($result)
                        {
                            $tabSubjPermEntity = array();
                            foreach($objSubjPermEntityCollection->getTabKey() as $strKey)
                            {
                                $objSubjPermEntity = $objSubjPermEntityCollection->getItem($strKey);
                                if(in_array($objSubjPermEntity->getAttributeValue(ConstPermission::ATTRIBUTE_KEY_ID), $tabId))
                                {
                                    $tabSubjPermEntity[] = $objSubjPermEntity;
                                }
                            }
                            $objSubjectSubjPermEntityCollection->setTabPermission($tabSubjPermEntity);
                        }
                    }
                    // Else: load subject permission collection
                    else
                    {
                        $result = $this->load($objSubjectSubjPermEntityCollection, $tabId) && $result;
                    }
                }
            }
        }

        // Reset global subject permission entity collection, if required
        if(!is_null($objSubjPermEntityCollection))
        {
            $objSubjPermEntityCollection->removeItemAll();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabSubjectInvalidFormatException
     */
    public function saveSubjectPermission($permissionSubject)
    {
        // Init var
        $tabPermissionSubject = (
            is_array($permissionSubject)?
                array_values($permissionSubject) :
                array($permissionSubject)
        );

        // Set check argument
        TabSubjectInvalidFormatException::setCheck($tabPermissionSubject);

        // Init var
        $result = true;
        $objSubjPermEntityCollection = $this->objSubjPermEntityCollection;

        // Set entity mapping
        $this->setSubjectEntityMap($tabPermissionSubject);

        // Clear global subject permission entity collection, if required
        if(!is_null($objSubjPermEntityCollection))
        {
            $objSubjPermEntityCollection->removeItemAll();
        }

        // Run each permission subject
        foreach($tabPermissionSubject as $objPermissionSubject)
        {
            /** @var PermissionSubjectInterface $objPermissionSubject */
            $objSubjectSubjPermEntityCollection = $objPermissionSubject->getObjPermissionCollection();

            // Check valid subject permission collection
            if($objSubjectSubjPermEntityCollection instanceof SubjPermEntityCollection)
            {
                // Build global subject permission entity collection, if required
                if(!is_null($objSubjPermEntityCollection))
                {
                    $objSubjPermEntityCollection->setTabItem($objSubjectSubjPermEntityCollection);
                    $result = $result && true;
                }
                // Else: save subject permission collection
                else
                {
                    $result = $this->save($objSubjectSubjPermEntityCollection) && $result;
                }
            }
        }

        // Save subject permission entity collection, if required
        if(!is_null($objSubjPermEntityCollection))
        {
            if($result)
            {
                $result = $this->save($objSubjPermEntityCollection) && $result;
            }

            // Reset global subject permission entity collection
            $objSubjPermEntityCollection->removeItemAll();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws TabSubjectInvalidFormatException
     */
    public function removeSubjectPermission($permissionSubject, $boolAll = true)
    {
        // Init var
        $tabPermissionSubject = (
            is_array($permissionSubject)?
                array_values($permissionSubject) :
                array($permissionSubject)
        );

        // Set check argument
        TabSubjectInvalidFormatException::setCheck($tabPermissionSubject);

        // Init var
        $result = true;
        $boolAll = (is_bool($boolAll) ? $boolAll : true);

        if(count($tabPermissionSubject) > 0)
        {
            // Init var
            $objSubjPermEntityCollection = $this->objSubjPermEntityCollection;

            // Set entity mapping
            $this->setSubjectEntityMap($tabPermissionSubject);

            // Clear global subject permission entity collection, if required
            if(!is_null($objSubjPermEntityCollection))
            {
                $objSubjPermEntityCollection->removeItemAll();
            }

            // Run each permission subject
            foreach($tabPermissionSubject as $objPermissionSubject)
            {
                /** @var PermissionSubjectInterface $objPermissionSubject */
                $objSubjectSubjPermEntityCollection = $objPermissionSubject->getObjPermissionCollection();

                // Check valid subject permission collection
                if($objSubjectSubjPermEntityCollection instanceof SubjPermEntityCollection)
                {
                    // Build global subject permission entity collection, if required
                    if(!is_null($objSubjPermEntityCollection))
                    {
                        $objSubjPermEntityCollection->setTabItem($objSubjectSubjPermEntityCollection);
                        $result = $result && true;
                    }
                    // Else: remove subject permission collection
                    else
                    {
                        $result = $this->remove($objSubjectSubjPermEntityCollection) && $result;
                    }
                }
            }

            // Remove global subject permission entity collection, if required
            if(!is_null($objSubjPermEntityCollection))
            {
                if($result)
                {
                    $result = $this->remove($objSubjPermEntityCollection) && $result;
                }

                // Reset global subject permission entity collection
                $objSubjPermEntityCollection->removeItemAll();
            }

            // Remove all subject permission entities, if required
            if($boolAll)
            {
                $result =
                    $this->removeSubjectPermissionAll($tabPermissionSubject) &&
                    $result;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Remove all permission entities, from specified permission subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * @param PermissionSubjectInterface[] $tabPermissionSubject
     * @return boolean
     */
    protected function removeSubjectPermissionAll(array $tabPermissionSubject)
    {
        // Init var
        $result = true;

        if(count($tabPermissionSubject) > 0)
        {
            // Init var
            $objDeleteCommand = $this
                ->getObjPersistor()
                ->getObjCommandFactory()
                ->getObjDeleteCommand();

            // Get SQL arguments
            $strTableNm = $this->getObjRepository()->getStrSqlTableName();
            $tabSqlConfigWhere = array_map(
                function($objPermissionSubject) {
                    /** @var PermissionSubjectInterface $objPermissionSubject */
                    return array(
                        'content' => [
                            [
                                'operand' => ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE,
                                'operator' => 'equal',
                                'value' => ['value' => $objPermissionSubject->getStrSubjectType()]
                            ],
                            [
                                'operand' => ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID,
                                'operator' => 'equal',
                                'value' => ['value' => $objPermissionSubject->getStrSubjectId()]
                            ]
                        ]
                    );
                },
                $tabPermissionSubject
            );

            // Build delete command
            $objDeleteCommand
                ->setClauseFrom(array($strTableNm))
                ->setClauseWhere(
                    array(
                        'type' => 'or',
                        'content' => $tabSqlConfigWhere
                    )
                );

            // Execute query
            $result = ($objDeleteCommand->execute() !== false);
        }

        // Return result
        return $result;
    }



}