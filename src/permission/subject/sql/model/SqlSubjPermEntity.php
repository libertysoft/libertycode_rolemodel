<?php
/**
 * This class allows to define SQL subject permission entity class.
 * SQL subject permission entity allows to design a subject permission entity class,
 * using SQL rules for attributes validation.
 *
 * SQL subject permission entity uses the following specified configuration:
 * [
 *     Subject permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\sql\model;

use liberty_code\role_model\permission\subject\model\SubjPermEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\subject\library\ConstSubjPerm;
use liberty_code\role_model\permission\subject\sql\model\repository\SqlSubjPermEntityRepository;



class SqlSubjPermEntity extends SubjPermEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * SQL subject permission entity repository instance.
     * @var null|SqlSubjPermEntityRepository
     */
    protected $objRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlSubjPermEntityRepository $objRepository = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        SqlSubjPermEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objDateTimeFactory
        );

        // Init SQL subject permission entity repository
        $this->setRepository($objRepository);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = parent::getTabRuleConfig();
        $objRepository = $this->getObjRepository();

        // Get rule configurations, if required
        if(!is_null($objRepository))
        {
            $result = array_merge(
                $result,
                array(
                    ConstPermission::ATTRIBUTE_KEY_ID => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-null' => [
                                        'is_null',
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return $this->checkIsNew();}
                                            ]
                                        ]
                                    ],
                                    'is-valid-id' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return (!$this->checkIsNew());}
                                            ]
                                        ],
                                        [
                                            'sql_exist',
                                            [
                                                'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                                'table_name' => $objRepository->getStrSqlTableName(),
                                                'column_name' => $this->getAttributeNameSave(ConstPermission::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                            ]
                        ]
                    ],
                    ConstPermission::ATTRIBUTE_KEY_KEY => [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'sql_exist',
                                        [
                                            'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                            'table_name' => $objRepository->getStrSqlTableName(),
                                            'column_name' => $this->getAttributeNameSave(ConstPermission::ATTRIBUTE_KEY_KEY),
                                            'include' => [
                                                $this->getAttributeNameSave(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE) =>
                                                    $this->getAttributeValueSave(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE),
                                                $this->getAttributeNameSave(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID) =>
                                                    $this->getAttributeValueSave(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID)
                                            ],
                                            'exclude' => [
                                                $this->getAttributeNameSave(ConstPermission::ATTRIBUTE_KEY_ID) =>
                                                    $this->getAttributeValueSave(ConstPermission::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be unique.'
                            ]
                        ]
                    ]
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get SQL subject permission entity repository object.
     *
     * @return null|SqlSubjPermEntityRepository
     */
    public function getObjRepository()
    {
        // Return result
        return $this->objRepository;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set SQL subject permission entity repository object.
     *
     * @param SqlSubjPermEntityRepository $objRepository = null
     */
    public function setRepository(SqlSubjPermEntityRepository $objRepository = null)
    {
        $this->objRepository = $objRepository;
    }



}