<?php
/**
 * This class allows to define SQL subject permission entity factory class.
 * SQL subject permission entity factory is subject permission entity factory,
 * to provide new SQL subject permission entities.
 *
 * SQL subject permission entity factory uses the following specified configuration, to get and hydrate subject permission:
 * [
 *     SQL subject permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\sql\model;

use liberty_code\role_model\permission\subject\model\SubjPermEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role_model\permission\subject\sql\model\SqlSubjPermEntity;
use liberty_code\role_model\permission\subject\sql\model\repository\SqlSubjPermEntityRepository;



/**
 * @method SqlSubjPermEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|SqlSubjPermEntity getObjPermission(array $tabConfig = array(), string $strConfigKey = null, PermissionInterface $objPermission = null) @inheritdoc
 */
class SqlSubjPermEntityFactory extends SubjPermEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SqlSubjPermEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SqlSubjPermEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objRepository = $this->getObjInstance(SqlSubjPermEntityRepository::class);
        $result = new SqlSubjPermEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objRepository
        );

        // Return result
        return $result;
    }



}