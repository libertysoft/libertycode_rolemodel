<?php
/**
 * This class allows to define subject permission entity collection repository class.
 * Subject permission entity collection repository allows to prepare data from subject permission entity collection,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\subject\api\PermissionSubjectInterface;
use liberty_code\role_model\permission\subject\library\ConstSubjPerm;
use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection;
use liberty_code\role_model\permission\subject\model\SubjPermEntityFactory;
use liberty_code\role_model\permission\subject\model\repository\SubjPermEntityRepository;



/**
 * @method null|SubjPermEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|SubjPermEntityRepository getObjRepository() @inheritdoc
 */
abstract class SubjPermEntityCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SubjPermEntityFactory $objEntityFactory = null
     * @param SubjPermEntityRepository $objRepository = null
     */
    public function __construct(
        SubjPermEntityFactory $objEntityFactory = null,
        SubjPermEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return SubjPermEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return SubjPermEntityRepository::class;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Update datetime create, update, on entities
        ToolBoxDateTime::hydrateEntityCollectionAttrDtCreateUpdate(
            $objCollection,
            ConstPermission::ATTRIBUTE_KEY_DT_CREATE,
            ConstPermission::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objCollection, $tabConfig, $tabInfo);
    }





    // Methods subject setters
    // ******************************************************************************

    /**
     * Set entity mapping,
     * from specified permission subjects,
     * to sub-element (permission subject permission entity collection).
     *
     * @param PermissionSubjectInterface[] $tabSubject
     * @return boolean
     */
    protected function setSubjectEntityMap(array $tabSubject)
    {
        // Init var
        $result = true;

        // Run each subject
        foreach($tabSubject as $objSubject)
        {
            // Get info
            $strSubjectType = $objSubject->getStrSubjectType();
            $strSubjectId = $objSubject->getStrSubjectId();
            $objSubjPermEntityCollection = $objSubject->getObjPermissionCollection();

            // Check valid permission collection
            if($objSubjPermEntityCollection instanceof SubjPermEntityCollection)
            {
                // Run each permission
                foreach($objSubjPermEntityCollection->getTabKey() as $strKey)
                {
                    $objSubjPerm = $objSubjPermEntityCollection->getItem($strKey);

                    // Set mapping for subject permission
                    $objSubjPerm->setAttributeValue(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE, $strSubjectType);
                    $objSubjPerm->setAttributeValue(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID, $strSubjectId);
                }
            }
            else
            {
                $result = false;
            }
        }

        // Return result
        return $result;
    }





    // Methods subject repository
    // ******************************************************************************

    /**
     * Load permission entities, on specified permission subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * @param PermissionSubjectInterface|PermissionSubjectInterface[] $permissionSubject
     * @return boolean
     */
    abstract public function loadSubjectPermission($permissionSubject);



    /**
     * Save permission entities, from specified permission subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * @param PermissionSubjectInterface|PermissionSubjectInterface[] $permissionSubject
     * @return boolean
     */
    abstract public function saveSubjectPermission($permissionSubject);



    /**
     * Remove permission entities, from specified permission subjects.
     * Return true if all subjects success, false if an error occurs on at least one subject.
     *
     * Remove all permission, from specified subjects, if option 'all' is true.
     * Else remove only permissions in collections, from specified subjects.
     *
     * @param PermissionSubjectInterface|PermissionSubjectInterface[] $permissionSubject
     * @param $boolAll = true
     * @return boolean
     */
    abstract public function removeSubjectPermission($permissionSubject, $boolAll = true);



}