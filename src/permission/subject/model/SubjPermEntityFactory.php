<?php
/**
 * This class allows to define subject permission entity factory class.
 * Subject permission entity factory is permission entity factory,
 * to provide new subject permission entities.
 *
 * Subject permission entity factory uses the following specified configuration, to get and hydrate subject permission:
 * [
 *     Subject permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\model;

use liberty_code\role_model\permission\model\PermissionEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role_model\permission\subject\model\SubjPermEntity;



/**
 * @method SubjPermEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|SubjPermEntity getObjPermission(array $tabConfig = array(), string $strConfigKey = null, PermissionInterface $objPermission = null) @inheritdoc
 */
class SubjPermEntityFactory extends PermissionEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SubjPermEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SubjPermEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new SubjPermEntity(
            array(),
            $objValidator,
            $objDateTimeFactory
        );

        // Return result
        return $result;
    }



}