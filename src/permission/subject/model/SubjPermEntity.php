<?php
/**
 * This class allows to define subject permission entity class.
 * Subject permission entity is permission entity,
 * related to a specific subject.
 *
 * Subject permission entity uses the following specified configuration:
 * [
 *     Permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\subject\model;

use liberty_code\role_model\permission\model\PermissionEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\role_model\permission\subject\library\ConstSubjPerm;



/**
 * @property null|string $strAttrSubjType
 * @property null|string $strAttrSubjId
 */
class SubjPermEntity extends PermissionEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute subject type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSubjPerm::ATTRIBUTE_ALIAS_SUBJ_TYPE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_TYPE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute subject id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSubjPerm::ATTRIBUTE_ALIAS_SUBJ_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstSubjPerm::ATTRIBUTE_NAME_SAVE_PERM_SUBJ_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => ['is_empty']
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a string not empty.'
                        ]
                    ]
                ],
                ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => ['is_empty']
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a string not empty.'
                        ]
                    ]
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE:
            case ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}