<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/role/permission/test/RolePermTest.php');

// Use
use liberty_code\role\permission\build\model\DefaultBuilder;
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;
use liberty_code\role_model\permission\subject\sql\model\SqlSubjPermEntityFactory;



// Init factory
$objSubjPermEntityFactory = new SqlSubjPermEntityFactory($objProvider);

// Init builder
$objSubjPermBuilder = new DefaultBuilder($objSubjPermEntityFactory);

$objSubjPermSpecBuilder = new PermSpecBuilder(
    $objPermSpec,
    $objSubjPermEntityFactory
);


