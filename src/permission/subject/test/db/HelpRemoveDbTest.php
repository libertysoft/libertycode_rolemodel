<?php
/**
 * GET arguments:
 * - clean: = 1: Drop test database at the end.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/db/HelpCreateDbTest.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\connection\library\ConstConnection;



// Remove database, if required
if(trim(ToolBoxTable::getItem($_GET, 'clean', '0')) == '1')
{
    // Re-connect to database
    $tabConfig = $objConnection->getTabConfig();
    unset($tabConfig[ConstConnection::TAB_CONFIG_KEY_DB_NAME]);
    $objConnection->setConfig($tabConfig);

    // Remove database
    $strSql = "DROP DATABASE " . $objConnection->getStrEscapeName($strDbNm) . ";";
    $objConnection->execute($strSql);

    echo(sprintf('Database "%s" deleted.', $strDbNm));
    echo('<br />' . PHP_EOL);
}



// Close connection
$objConnection->close();



echo('<br /><br /><br />' . PHP_EOL . PHP_EOL . PHP_EOL);


