<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/db/DbTest.php');

// Use
use liberty_code\sql\database\connection\library\ConstConnection;



// Check database exists
$boolFind = false;
$strSql = "SHOW DATABASES;";
$objResult = $objConnection->executeResult($strSql);
while((($data = $objResult->getFetchData()) !== false) && (!$boolFind))
{
    $boolFind = ($data['Database'] == $strDbNm);
}

$objResult->close();



// Create database, if required
if(!$boolFind)
{
    $strSql = "CREATE DATABASE " . $objConnection->getStrEscapeName($strDbNm) . ";";
    $objConnection->execute($strSql);

    echo(sprintf('Database "%s" created.', $strDbNm));
    echo('<br />' . PHP_EOL);
}



// Re-connect to database
$tabConfig[ConstConnection::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;
$objConnection->setConfig($tabConfig);



echo('<br /><br /><br />' . PHP_EOL . PHP_EOL . PHP_EOL);


