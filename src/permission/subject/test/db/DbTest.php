<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load test
require_once($strRootAppPath . '/vendor/liberty_code/sql/src/database/test/MysqlPdoConnection.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\pdo\library\ConstPdoConnection;
use liberty_code\sql\database\test\MysqlPdoConnection;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\sql\browser\table\model\TableBrowser;



// Init var
$strDbNm = 'libertycode_sql_test';

$tabConfig = array(
    ConstConnection::TAB_CONFIG_KEY_HOST => 'localhost',
    ConstConnection::TAB_CONFIG_KEY_CHARSET => 'utf8',
    ConstConnection::TAB_CONFIG_KEY_LOGIN => 'root',
    ConstConnection::TAB_CONFIG_KEY_PASSWORD => '',
    ConstPdoConnection::TAB_CONFIG_KEY_OPTION => [
        //PDO::ATTR_AUTOCOMMIT => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT
    ]
);



// Init DB object
$objConnection = new MysqlPdoConnection($tabConfig);
$objCommandFacto = new StandardCommandFactory($objConnection);
$objTablePersistor = new TablePersistor($objCommandFacto);
$objTableBrowser = new TableBrowser(
    new OperatorData(),
    $objCommandFacto,
    new DefaultTableRegister()
);


