<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/SubjPermRepositoryTest.php');

// Use
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection;



// Init var
$objSubjPermEntityCollection = new SubjPermEntityCollection();



// Test hydrate subject permission entity collection
echo('Test hydrate subject permission entity collection: <br />');

echo('Before hydrate: <pre>');
foreach($objSubjPermEntityCollection->getTabKey() as $strKey)
{
    var_dump($objSubjPermEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

$objSubjPermSpecBuilder->hydratePermissionCollection($objSubjPermEntityCollection);

echo('After hydrate: <pre>');
foreach($objSubjPermEntityCollection->getTabKey() as $strKey)
{
    var_dump($objSubjPermEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');



// Test get
echo('Test get subject permission: <br />');

try
{
    $objSubjPermEntityCollection->setPermissionAllEnable('test');
}
catch (Exception $e)
{
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}
$objSubjPermEntityCollection->setPermissionAllEnable(true);

foreach($objSubjPermEntityCollection->getTabPermissionKey() as $strPermissionKey)
{
    echo('Test get subject permission key "'.$strPermissionKey.'": <br />');

    $objSubjPermEntity = $objSubjPermEntityCollection->getObjPermission($strPermissionKey);

    echo('Get subject permission key: <pre>');var_dump($objSubjPermEntity->getStrPermissionKey());echo('</pre>');
    echo('Check subject permission enable: <pre>');var_dump($objSubjPermEntity->checkPermissionEnable());echo('</pre>');
    echo('Get subject permission configuration: <pre>');var_dump($objSubjPermEntity->getTabPermissionConfig());echo('</pre>');
    echo('Check collection subject permission enable: <pre>');var_dump($objSubjPermEntityCollection->checkPermissionEnable($strPermissionKey));echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test re-hydrate subject permission entity collection
echo('Test re-hydrate subject permission entity collection: <br />');

$tabSubjPermEntity = array(
    $objSubjPermEntityFactory->getObjPermission(array(
        ConstPermission::ATTRIBUTE_KEY_KEY => 'test-add-1',
        ConstPermission::ATTRIBUTE_KEY_VALUE => true
    )),
    $objSubjPermEntityFactory->getObjPermission(array(
        ConstPermission::ATTRIBUTE_KEY_KEY => 'test-add-2'
    )),
    $objSubjPermEntityFactory->getObjPermission(array(
        ConstPermission::ATTRIBUTE_KEY_KEY => 'test-add-3',
        ConstPermission::ATTRIBUTE_KEY_VALUE => true
    ))
);
$objSubjPermEntityCollection->setTabPermission($tabSubjPermEntity);

echo('Before hydrate: <pre>');
foreach($objSubjPermEntityCollection->getTabKey() as $strKey)
{
    var_dump($objSubjPermEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

$objSubjPermSpecBuilder->hydratePermissionCollection($objSubjPermEntityCollection, false);

echo('After hydrate: <pre>');
foreach($objSubjPermEntityCollection->getTabKey() as $strKey)
{
    var_dump($objSubjPermEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');


