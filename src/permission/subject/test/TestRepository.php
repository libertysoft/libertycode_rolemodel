<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/SubjPermRepositoryTest.php');
require_once($strRootAppPath . '/src/permission/subject/test/PermissionSubjectTest.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\subject\library\ConstSubjPerm;
use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection;
use liberty_code\role_model\permission\subject\test\PermissionSubjectTest;



// Init var
$getStrSubjectRender = function(PermissionSubjectTest $objSubject)
{
    $result = sprintf(
        'Subject (type: "%1$s" - id: "%2$s"):<br />',
        $objSubject->getStrSubjectType(),
        $objSubject->getStrSubjectId()
    );

    /** @var SubjPermEntityCollection $objSubjPermEntityCollection */
    $objSubjPermEntityCollection = $objSubject->getObjPermissionCollection();
    foreach($objSubjPermEntityCollection->getTabPermissionKey() as $strPermissionKey)
    {
        $objSubjPermEntity = $objSubjPermEntityCollection->getObjPermission($strPermissionKey);
        $result .= sprintf(
            'Permission "%1$s"%2$s (Subject type: "%3$s" / Subject id: "%4$s"): %5$s<br />',
            $objSubjPermEntity->getStrPermissionKey(),
            ($objSubjPermEntity->checkIsNew() ? ' [NEW]' : ''),
            strval($objSubjPermEntity->getAttributeValue(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE)),
            strval($objSubjPermEntity->getAttributeValue(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID)),
            ($objSubjPermEntity->checkPermissionEnable() ? '1' : '0')
        );
    }

    return $result;
};

$getStrTabSubjectRender = function(array $tabSubject) use ($getStrSubjectRender)
{
    $result = '';

    foreach($tabSubject as $objSubject)
    {
        /** @var PermissionSubjectTest $objSubject */
        $result .= sprintf(
            '%1$s<br /><br />',
            $getStrSubjectRender($objSubject)
        );
    }

    return $result;
};

$objSubject1 = new PermissionSubjectTest(
    '1',
    new SubjPermEntityCollection()
);

$objSubject2 = new PermissionSubjectTest(
    '2',
    new SubjPermEntityCollection()
);

$objSubject3 = new PermissionSubjectTest(
    '3',
    new SubjPermEntityCollection()
);

$objSubject4 = new PermissionSubjectTest(
    '4',
    new SubjPermEntityCollection()
);

/** @var PermissionSubjectTest[] $tabSubject */
$tabSubject = array(
    $objSubject1,
    $objSubject2,
    $objSubject3,
    $objSubject4
);



// Init structure
echo('Test set structure: <pre>');
var_dump($objSubjPermEntityRepo->setStructure());
echo('</pre>');

echo('<br /><br /><br />');



// Load subjects permissions
echo('Test load subjects permissions: <pre>');
var_dump($objSubjPermEntityCollectionRepo->loadSubjectPermission($tabSubject));
echo('</pre>');

foreach($tabSubject as $objSubject)
{
    $objSubjPermSpecBuilder->hydratePermissionCollection($objSubject->getObjPermissionCollection(), false);
}

echo($getStrTabSubjectRender($tabSubject));

echo('<br /><br /><br />');



// Remove subjects permissions
echo('Test remove subjects permissions: <pre>');
var_dump($objSubjPermEntityCollectionRepo->removeSubjectPermission($tabSubject));
echo('</pre>');

echo($getStrTabSubjectRender($tabSubject));

echo('<br /><br /><br />');



// Save subjects permissions
foreach($tabSubject as $objSubject)
{
    /** @var SubjPermEntityCollection $objSubjPermEntityCollection */
    $objSubjPermEntityCollection = $objSubject->getObjPermissionCollection();
    foreach($objSubjPermEntityCollection->getTabPermissionKey() as $strPermissionKey)
    {
        $objSubjPermEntity = $objSubjPermEntityCollection->getObjPermission($strPermissionKey);

        if($objSubjPermEntity->checkIsNew())
        {
            $objSubjPermEntity->setAttributeValue(ConstPermission::ATTRIBUTE_KEY_ID, null);
        }

        $boolEnable = ((rand(0, 10) % 2) == 0);
        //$objSubjPermEntity->setAttributeValue(ConstPermission::ATTRIBUTE_KEY_ID, 7);
        //$objSubjPermEntity->setAttributeValue(ConstPermission::ATTRIBUTE_KEY_KEY, 'test_get');
        $objSubjPermEntity->setPermissionEnable($boolEnable);
        $objSubjPermEntity->setAttributeValue(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_TYPE, null);
        $objSubjPermEntity->setAttributeValue(ConstSubjPerm::ATTRIBUTE_KEY_SUBJ_ID, null);
    }
}

echo('Test save subjects permissions: ');

// Run each subject entity
$boolValid = true;
$tabError = array();
foreach($tabSubject as $objSubject)
{
    /** @var SubjPermEntityCollection $objSubjPermEntityCollection */
    $objSubjPermEntityCollection = $objSubject->getObjPermissionCollection();

    // Check validation
    $tabSubjPermEntityCollectionError = array();
    $boolValid =
        $objSubjPermEntityCollection->checkValid(null, null, $tabSubjPermEntityCollectionError) &&
        $boolValid;

    // Get errors
    $tabError = ToolBoxTable::getTabMerge($tabError, $tabSubjPermEntityCollectionError);
}

if($boolValid)
{
    echo('<pre>');var_dump($objSubjPermEntityCollectionRepo->saveSubjectPermission($tabSubject));echo('</pre>');
}
else
{
    echo('Validation failed:<pre>');var_dump($tabError);echo('</pre>');
}

echo($getStrTabSubjectRender($tabSubject));

echo('<br /><br /><br />');



// Remove test database, if required
require_once($strRootAppPath . '/src/permission/subject/test/db/HelpRemoveDbTest.php');


