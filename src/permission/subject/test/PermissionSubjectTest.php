<?php

namespace liberty_code\role_model\permission\subject\test;

use liberty_code\role\role\subject\model\DefaultRolePermissionSubject;
use liberty_code\role_model\permission\subject\api\PermissionSubjectInterface;

use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\role\api\RoleCollectionInterface;



/**
 * @method void setStrSubjectId(string $strSubjectId) Set subject id.
 */
class PermissionSubjectTest extends DefaultRolePermissionSubject implements PermissionSubjectInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param string $strSubjectId
     */
    public function __construct(
        $strSubjectId,
        PermissionCollectionInterface $objPermissionCollection = null,
        RoleCollectionInterface $objRoleCollection = null
    )
    {
        // Call parent constructor
        parent::__construct($objPermissionCollection, $objRoleCollection);

        // Init subject id
        $this->setStrSubjectId($strSubjectId);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists('strSubjectId'))
        {
            $this->__beanTabData['strSubjectId'] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            'strSubjectId'
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case 'strSubjectId':
                    $result = is_string($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrSubjectType()
    {
        // Return result
        return 'test_subject';
    }



    /**
     * @inheritdoc
     */
    public function getStrSubjectId()
    {
        // Return result
        return $this->beanGet('strSubjectId');
    }



}