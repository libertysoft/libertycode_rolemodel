<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/permission/subject/test/db/HelpCreateDbTest.php');
require_once($strRootAppPath . '/src/permission/subject/test/SubjPermTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;

use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection;
use liberty_code\role_model\permission\subject\sql\model\repository\SqlSubjPermEntityRepository;
use liberty_code\role_model\permission\subject\sql\model\repository\SqlSubjPermEntityCollectionRepository;



// Init repository
$objSubjPermEntityRepo = new SqlSubjPermEntityRepository($objTablePersistor);

$objPref = new Preference(array(
    'source' => SqlSubjPermEntityRepository::class,
    'set' =>  ['type' => 'instance', 'value' => $objSubjPermEntityRepo],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

// Init collection repository
$objSubjPermEntityCollectionRepo = new SqlSubjPermEntityCollectionRepository(
    $objSubjPermEntityFactory,
    $objSubjPermEntityRepo,
    new SubjPermEntityCollection()
);


