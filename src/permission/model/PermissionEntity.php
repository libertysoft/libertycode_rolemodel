<?php
/**
 * This class allows to define permission entity class.
 * Permission entity allows to design a permission class,
 * using entity.
 *
 * Permission entity uses the following specified configuration:
 * [
 *     @see PermissionEntity::hydrate():
 *         List of attributes hydration: @see PermissionEntity::getTabConfig()
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\role\permission\api\PermissionInterface;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role_model\permission\library\ConstPermission;



/**
 * @property integer $intAttrId
 * @property string|DateTime $attrDtCreate
 * @property string|DateTime $attrDtUpdate
 * @property string $strAttrKey
 * @property boolean $boolAttrValue
 */
abstract class PermissionEntity extends SaveConfigEntity implements PermissionInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Call parent constructor
        parent::__construct($tabValue, $objValidator);

        // Init datetime factory repository
        $this->setDateTimeFactory($objDateTimeFactory);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstPermission::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();
        $objDtNow = new DateTime();
        $objDtNow = (
            (!is_null($objDtFactory)) ?
                $objDtFactory->getObjRefDt($objDtNow) :
                $objDtNow
        );

        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermission::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermission::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermission::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermission::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $objDtNow
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermission::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermission::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => $objDtNow
            ],

            // Attribute key
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermission::ATTRIBUTE_KEY_KEY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermission::ATTRIBUTE_ALIAS_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ],

            // Attribute value
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstPermission::ATTRIBUTE_KEY_VALUE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstPermission::ATTRIBUTE_ALIAS_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstPermission::ATTRIBUTE_NAME_SAVE_PERM_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => false
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            ConstPermission::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                    ]
                ]
            ],
            ConstPermission::ATTRIBUTE_KEY_DT_CREATE => [
                'type_date'
            ],
            ConstPermission::ATTRIBUTE_KEY_DT_UPDATE => [
                'type_date'
            ],
            ConstPermission::ATTRIBUTE_KEY_KEY => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ],
            ConstPermission::ATTRIBUTE_KEY_VALUE => [
                [
                    'type_boolean',
                    [
                        'integer_enable_require' => false,
                        'string_enable_require' => false
                    ]
                ]
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstPermission::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermission::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstPermission::ATTRIBUTE_KEY_ID:
                $result = (
                    (is_string($value) && ctype_digit($value)) ?
                        intval($value) :
                        $value
                );
                break;

            case ConstPermission::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermission::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSet($value, true) :
                        (
                            ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                $objDtFactory->getObjRefDt($value) :
                                $value
                        )
                );
                break;

            case ConstPermission::ATTRIBUTE_KEY_VALUE:
                $result = (
                    (
                        (
                            is_int($value) ||
                            (is_string($value) && ctype_digit($value))
                        ) &&
                        in_array(intval($value), array(0, 1))
                    ) ?
                        (intval($value) === 1) :
                        $value
                );
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstPermission::ATTRIBUTE_KEY_ID:
                $result = strval($value);
                break;

            case ConstPermission::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermission::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstPermission::ATTRIBUTE_KEY_VALUE:
                $result = ((is_bool($value) && $value) ? '1' : '0');
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->getObjDateTimeFactory();

        // Format by attribute
        switch($strKey)
        {
            case ConstPermission::ATTRIBUTE_KEY_ID:
                $result = intval($value);
                break;

            case ConstPermission::ATTRIBUTE_KEY_DT_CREATE:
            case ConstPermission::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        (
                            ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                $objDtFactory->getObjRefDt($value) :
                                $value
                        )
                );
                break;

            case ConstPermission::ATTRIBUTE_KEY_VALUE:
                $result = ($value == '1');
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable()
    {
        // Return result
        return $this->getAttributeValue(ConstPermission::ATTRIBUTE_KEY_VALUE, false);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get datetime factory object.
     *
     * @return null|DateTimeFactoryInterface
     */
    public function getObjDateTimeFactory()
    {
        // Return result
        return $this->objDateTimeFactory;
    }



    /**
     * @inheritdoc
     */
    public function getStrPermissionKey()
    {
        // Return result
        return $this->getAttributeValue(ConstPermission::ATTRIBUTE_KEY_KEY);
    }



    /**
     * Get formatted permission configuration array.
     *
     * @param array $tabConfig
     * @return array
     */
    protected function getTabPermissionConfigFormat(array $tabConfig)
    {
        // Init var
        $result = array();

        // Map specific attributes
        $tabAttrKey = array(
            ConstPermission::ATTRIBUTE_KEY_KEY,
            ConstPermission::ATTRIBUTE_KEY_VALUE
        );
        foreach($tabAttrKey as $strAttrKey)
        {
            if(array_key_exists($strAttrKey, $tabConfig))
            {
                $result[$strAttrKey] = $tabConfig[$strAttrKey];
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabPermissionConfig()
    {
        // Init var
        $result = $this->getTabData(false);
        $result = $this->getTabPermissionConfigFormat($result);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set datetime factory object.
     *
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function setDateTimeFactory(DateTimeFactoryInterface $objDateTimeFactory = null)
    {
        $this->objDateTimeFactory = $objDateTimeFactory;
    }



    /**
     * @inheritdoc
     */
    public function setPermissionConfig(array $tabConfig)
    {
        // Init var
        $tabConfig = $this->getTabPermissionConfigFormat($tabConfig);

        // Hydrate attributes
        $this->hydrate($tabConfig);
    }



    /**
     * Set permission enabled.
     *
     * @param boolean $boolEnable
     */
    public function setPermissionEnable($boolEnable)
    {
        // Set value
        $this->setAttributeValue(ConstPermission::ATTRIBUTE_KEY_VALUE, $boolEnable);
    }



}