<?php
/**
 * This class allows to define permission entity factory class.
 * Permission entity factory allows to design a permission factory class,
 * to provide new permission entities.
 *
 * Permission entity factory uses the following specified configuration, to get and hydrate permission:
 * [
 *     Permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;
use liberty_code\role\permission\factory\api\PermissionFactoryInterface;

use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role_model\permission\library\ConstPermission;
use liberty_code\role_model\permission\model\PermissionEntity;



/**
 * @method PermissionEntity getObjEntity(array $tabValue = array(), boolean $boolCheckExist = true) @inheritdoc
 */
class PermissionEntityFactory extends FixEntityFactory implements PermissionFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => PermissionEntity::class
        );
    }



    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = $tabConfig;

        // Add configured key as permission key, if required
        if(
            (!is_null($strConfigKey)) &&
            (!array_key_exists(ConstPermission::ATTRIBUTE_KEY_KEY, $result))
        )
        {
            $result[ConstPermission::ATTRIBUTE_KEY_KEY] = $strConfigKey;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrPermissionClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Return result
        return $this->getStrEntityClassPath();
    }



    /**
     * @inheritdoc
     * @return PermissionEntity
     */
    public function getObjPermission(
        array $tabConfig,
        $strConfigKey = null,
        PermissionInterface $objPermission = null
    )
    {
        // Init var
        $strClassPath = $this->getStrEntityClassPath();
        $result = (
            is_null($objPermission) ?
                $this->getObjEntity() :
                (
                    (
                        is_subclass_of($objPermission, $strClassPath) ||
                        get_class($objPermission) == $strClassPath
                    ) ?
                        $objPermission :
                        null
                )
        );

        // Hydrate permission entity, if required
        if(!is_null($result))
        {
            $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
            $result->setPermissionConfig($tabConfigFormat);
        }

        // Return result
        return $result;
    }



}