<?php
/**
 * This class allows to define permission entity collection class.
 * Permission entity collection allows to design a permission collection class,
 * using entity collection.
 * key: permission key => permission entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\permission\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;
use liberty_code\role\permission\api\PermissionCollectionInterface;

use liberty_code\model\entity\exception\CollectionValueInvalidFormatException;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role_model\permission\model\PermissionEntity;
use liberty_code\role_model\permission\exception\PermissionKeyNotFoundException;



/**
 * @method null|PermissionEntity getItem(string $strKey) @inheritdoc
 * @method string setItem(PermissionEntity $objEntity) @inheritdoc
 */
class PermissionEntityCollection extends FixEntityCollection implements PermissionCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjPermission($strKey)));
    }



    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Init var
        $objPermission = $this->getObjPermission($strKey);
        $result = (
            (!is_null($objPermission)) ?
                $objPermission->checkPermissionEnable() :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return PermissionEntity::class;
    }



    /**
     * Get permission entity collection key,
     * from specified permission key.
     *
     * @param string $strPermissionKey
     * @param boolean $boolThrow = true
     * @return null|string
     * @throws PermissionKeyNotFoundException
     */
    protected function getStrKeyFromPermissionKey($strPermissionKey, $boolThrow = true)
    {
        // Init var
        $result = null;
        $tabKey = $this->getTabKey();

        // Run each entities
        for($intCpt = 0; ($intCpt < count($tabKey)) && is_null($result); $intCpt++)
        {
            $strKey = $tabKey[$intCpt];
            $objPermissionEntity = $this->getItem($strKey);

            // Register key, if found
            $result = (
                (
                    is_string($strPermissionKey) &&
                    ($objPermissionEntity->getStrPermissionKey() == $strPermissionKey)
                ) ?
                    $strKey :
                    null
            );
        }

        // Throw exception, if required
        if(is_null($result) && $boolThrow)
        {
            throw new PermissionKeyNotFoundException($strPermissionKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get permission key,
     * from specified permission entity collection key.
     *
     * @param string $strKey
     * @return null|string
     */
    protected function getStrPermissionKeyFromKey($strKey)
    {
        // Init var
        $result = null;
        $objPermissionEntity = $this->getItem($strKey);
        $result = (
            (!is_null($objPermissionEntity)) ?
                $objPermissionEntity->getStrPermissionKey() :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabPermissionKey()
    {
        // Init var
        $result = $this->getTabKey();
        $result = array_map(
            function($strKey)
            {
                return $this->getStrPermissionKeyFromKey($strKey);
            },
            $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return null|PermissionEntity
     */
    public function getObjPermission($strKey)
    {
        // Init var
        $strKey = $this->getStrKeyFromPermissionKey($strKey, false);
        $result = (
            (!is_null($strKey)) ?
                $this->getItem($strKey) :
                null
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionEntity $objPermission
     * @throws CollectionValueInvalidFormatException
     */
    public function setPermission(PermissionInterface $objPermission)
    {
        // Check argument
        if(!($objPermission instanceof PermissionEntity))
        {
            throw new CollectionValueInvalidFormatException($objPermission);
        }

        // Init var
        /** @var PermissionEntity $objPermission */
        $strKey = $this->setItem($objPermission);
        $result = $this->getStrPermissionKeyFromKey($strKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param array|static $tabPermission
     */
    public function setTabPermission($tabPermission)
    {
        // Init var
        $tabKey = $this->setTabItem($tabPermission);
        $result = array_map(
            function($strKey)
            {
                return $this->getStrPermissionKeyFromKey($strKey);
            },
            $tabKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return PermissionEntity
     */
    public function removePermission($strKey)
    {
        // Init var
        $strKey = $this->getStrKeyFromPermissionKey($strKey);

        // return result
        return $this->removeItem($strKey);
    }



    /**
     * @inheritdoc
     */
    public function removePermissionAll()
    {
        $this->removeItemAll();
    }



    /**
     * Set all permissions enabled.
     *
     * @param boolean $boolEnable
     */
    public function setPermissionAllEnable($boolEnable)
    {
        // Run all permissions
        foreach($this->getTabPermissionKey() as $strKey)
        {
            $objPermission = $this->getObjPermission($strKey);

            // Set permission enabled
            $objPermission->setPermissionEnable($boolEnable);
        }
    }



}