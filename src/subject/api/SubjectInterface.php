<?php
/**
 * Description :
 * This class allows to describe behavior of subject class.
 * Subject can be used as subject base, for role and permission entities.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role_model\subject\api;

use liberty_code\role\subject\api\SubjectInterface as BaseSubjectInterface;



interface SubjectInterface extends BaseSubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get subject type,
     * used in repository to be saved.
     *
     * @return string
     */
    public function getStrSubjectType();



    /**
     * Get subject id (used as identifier, for same type),
     * used in repository to be saved.
     *
     * @return string
     */
    public function getStrSubjectId();
}